﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneFader : MonoBehaviour
{
    public Image img;
    public AnimationCurve curve;

    void Start()
    {
        StartCoroutine(FadeIn());
    }

    public void FadeTo(string scene)
    {
        StartCoroutine(FadeOut(scene));
    }

    // real function for fading in
    IEnumerator FadeInProper()
    {
        float t = 1f;

        while (t > 0f)
        {
            t -= Time.deltaTime;        // wait a single frame
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;             // skip a frame
        }
    }

    IEnumerator FadeIn()
    {
        // fade in first
        yield return StartCoroutine(FadeInProper());

        string scene = SceneManager.GetActiveScene().name;
        // so that timeScale = 0 when level starts
        // while journal is up
        // update Contains as new type of levels added i.e. "OR" other drugs
        /* if (scene.Contains("Alcohol") && !scene.Contains("Select")) 
        {
            Time.timeScale = 0f;
        }    */  
    }

    IEnumerator FadeOut(string scene)
    {
        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime;
            float a = curve.Evaluate(t);
            img.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }

        SceneManager.LoadScene(scene);
    }

}
