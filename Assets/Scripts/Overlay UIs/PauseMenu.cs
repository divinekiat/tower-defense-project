﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{ 
    public GameObject ui;

    public string menuSceneName = "MainMenu";

    public SceneFader sceneFader;
    public Text currentScore;

    private WaveSpawner waveSpawner;

    private void Start()
    {
        waveSpawner = GetComponent<WaveSpawner>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            Toggle();
        }

        currentScore.text = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizScore", 0)
                            + "/" + waveSpawner.waves.Length;
        
    }

    public void Toggle()
    {
        ui.SetActive(!ui.activeSelf);

        if (ui.activeSelf)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = FastForward.gameSpeedScale;
        }
    }

    public void Retry()
    {
        Toggle();
        Time.timeScale = 1f;
 
        sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        Toggle();
        Time.timeScale = 1f;

        sceneFader.FadeTo(menuSceneName);
    }

}
