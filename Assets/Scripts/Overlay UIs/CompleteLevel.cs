﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour
{

    public string menuSceneName = "MainMenu";

    public string nextLevel = "Alcohol02";
    public int levelToUnlock = 2;

    private SceneFader sceneFader;

    void OnEnable()
    {
        sceneFader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
        PlayerPrefs.SetInt("levelReached", levelToUnlock);
    }

    public void Continue()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(nextLevel);
    }

    public void Menu()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(menuSceneName);
    }
}
