﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    // public Text roundsText;

    public string menuSceneName = "MainMenu";

    private SceneFader sceneFader;
    private WaveSpawner waveSpawner;

    void Awake()
    {
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
        sceneFader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
    }

    void OnEnable()
    {
        
        
        // roundsText.text = PlayerStats.Rounds.ToString();
    }

    public void Retry()
    {
        Time.timeScale = 1f;    // ensures Fast-forward will stop
 
        sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void Menu()
    {
        Time.timeScale = 1f; // ensures Fast-forward will stop
 
        sceneFader.FadeTo(menuSceneName);
    }
}
