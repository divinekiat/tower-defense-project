﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundsSurvived : MonoBehaviour
{

    public bool isLevelComplete;

    public Text roundsText;

    void OnEnable()
    {
        StartCoroutine(AnimateText());
    }

    IEnumerator AnimateText()
    {
        roundsText.text = "0";
        int round = 0;

        yield return new WaitForSeconds(.7f);

        int noOfRounds = 0;

        if (isLevelComplete)
            noOfRounds = PlayerStats.Rounds;
        else
            noOfRounds = PlayerStats.Rounds - 1;    // since current wave isn't finished

        while (round < noOfRounds)
        {
            round++;
            roundsText.text = round.ToString();

            yield return new WaitForSeconds(.05f);
        }

    }

}
