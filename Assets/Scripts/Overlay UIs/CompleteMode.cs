﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteMode : MonoBehaviour
{

    public string menuSceneName = "MainMenu";
    public string modeName = "ALCOHOL";

    private SceneFader sceneFader;

    void Awake()
    {
        sceneFader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
    }

    public void Menu()
    {
        Time.timeScale = 1f;
        sceneFader.FadeTo(menuSceneName);
    }
}
