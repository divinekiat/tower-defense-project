﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JournalUI : MonoBehaviour {

    public static JournalUI instance;

    void Awake()
    {
        // PlayerPrefs.DeleteAll(); // for debugging purposes
        if (instance != null)
        {
            Debug.LogError("More than one JournalUI in scene!");
            return;
        }
        instance = this;
    }

    public GameObject journalUI;
    public static bool journalIsActive = true;

    public Book book;

    public GameObject[] pageSpreads;
    public int[] numberOfEntriesOnPageSpread;
    private int pageIndex;

    public GameObject[] entryPanels;

    private Text[] dates;
    private Text[] entryTexts; 

    public static int entryIndex;   // entry as in JOURNAL entry

    public bool afterClosed = false;

	void Start () {

        pageIndex = 0;
        entryIndex = PlayerPrefs.GetInt("entryIndex", -1);

        dates = new Text[entryPanels.Length];
        entryTexts = new Text[entryPanels.Length];

        /*for (int i = 0; i < entryPanels.Length; i++)
        {
            dates[i] = entryPanels[i].transform.Find("Date").GetComponent<Text>();
            entryTexts[i] = entryPanels[i].transform.Find("EntryText").GetComponent<Text>();
            entryPanels[i].SetActive(false);
        }*/

        if(entryIndex < entryPanels.Length)
        {
            for (int i = 0; i <= entryIndex; i++)
            {
                entryPanels[i].SetActive(true);
            }
        }

        WriteJournalEntry();
    }

    void Update()
    {
        // debugging WriteJournalEntry
        if (Input.GetKeyDown("n"))
        {
            WriteJournalEntry();
        }
    }

    public void OpenJournalUI()
    {
        journalIsActive = true;
        journalUI.GetComponent<Canvas>().enabled = true;
    }

    public void CloseJournalUI()
    {
        Time.timeScale = FastForward.gameSpeedScale;
        afterClosed = true;
        journalIsActive = false;
        journalUI.GetComponent<Canvas>().enabled = false;
    }

    public void WriteJournalEntry()
    {
        entryIndex++;
        PlayerPrefs.SetInt("entryIndex", entryIndex);
        SetCorrectPage();

        if (entryIndex >= entryPanels.Length)
            return;

        entryPanels[entryIndex].SetActive(true);

        string entryTextString = entryTexts[entryIndex].text;
        entryTexts[entryIndex].text = "";

        string dateString = dates[entryIndex].text;
        dates[entryIndex].text = "";

        StartCoroutine(AnimateTextForEntry(dates[entryIndex], dateString, entryTexts[entryIndex], entryTextString));
    }

    // typing animation
    IEnumerator AnimateText(Text text, string textString)
    {   
        foreach(char letter in textString.ToCharArray())
        {
            text.text += letter;
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    // typing animation for entry
    IEnumerator AnimateTextForEntry(Text dateText, 
                                    string dateTextString,
                                    Text entryText,
                                    string entryTextString)
    {
        foreach (char letter in dateTextString.ToCharArray())
        {
            dateText.text += letter;
            yield return new WaitForSecondsRealtime(0.01f);
        }

        foreach (char letter in entryTextString.ToCharArray())
        {
            entryText.text += letter;
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    public void NextPage()
    {
        if (pageSpreads[pageIndex + 1] == null)
            return;

        pageSpreads[pageIndex].SetActive(false);
        pageIndex++;
        pageSpreads[pageIndex].SetActive(true);
    }

    public void PreviousPage()
    {
        if (pageSpreads[pageIndex - 1] == null)
            return;

        pageSpreads[pageIndex].SetActive(false);
        pageIndex--;
        pageSpreads[pageIndex].SetActive(true);
    }

    public void SetCorrectPage()
    {
        int[] totalEntriesOnPageSpreads = new int[pageSpreads.Length];
        for (int i = 0; i < pageSpreads.Length; i++)
        {
            if (i == 0)
                totalEntriesOnPageSpreads[i] = numberOfEntriesOnPageSpread[i];
            else
                totalEntriesOnPageSpreads[i] = numberOfEntriesOnPageSpread[i] + totalEntriesOnPageSpreads[i - 1];

        }

        for (int i = 0; i < pageSpreads.Length; i++)
        {
            if (entryIndex < totalEntriesOnPageSpreads[i])
            {
                SetPage(i);
                return;
            }

        }
    }

    public void SetPage(int index)
    {
        pageIndex = index;
        foreach (GameObject pageSpread in pageSpreads) {
            pageSpread.SetActive(false);
        }

        pageSpreads[index].SetActive(true);
    }

    // For retrying purposes... 
    // so no new journal entry by the start of level
    // i.e. same old journal entry
    public void ResetEntryIndex()
    {
        entryIndex--;
        PlayerPrefs.SetInt("entryIndex", entryIndex);
    }
}
