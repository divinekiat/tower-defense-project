﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalJournalUI : MonoBehaviour {

    public static FinalJournalUI instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one NewJournalUI in scene!");
            return;
        }
        instance = this;
    }

    public Image journalImage;
    public Button nextButton;
    public Button previousButton;
    private GameManager gameManager;

    public Sprite[] journalEntrySprites;
    private int pageIndex;

    [HideInInspector]
    public bool afterClosed = false;

    void Start () {
        gameManager = GameObject.Find("GameMaster").GetComponent<GameManager>();
        pageIndex = journalEntrySprites.Length - 1;
        SetPage(pageIndex);
    }
    
    void Update()
    {
        if (pageIndex + 1 >= journalEntrySprites.Length)
            nextButton.interactable = false;
        else
            nextButton.interactable = true;

        if (pageIndex - 1 < 0)
            previousButton.interactable = false;
        else
            previousButton.interactable = true;
    }

    public void OpenJournalUI()
    {
        GetComponent<Canvas>().enabled = true;
    }

    public void CloseJournalUI()
    {
        Time.timeScale = FastForward.gameSpeedScale;
        afterClosed = true;
        GetComponent<Canvas>().enabled = false;

        int currentAlcoholScore = PlayerPrefs.GetInt("AlcoholScore");
        float quizPercentage = currentAlcoholScore / (gameManager.totalQuizHPS * 1.0f);

        WaveSpawner waveSpawner = gameManager.gameObject.GetComponent<WaveSpawner>();

        if (quizPercentage >= 0.83)
            waveSpawner.doctorTrigger.TriggerEndingDialogue(2);
        else if (quizPercentage >= 0.53)
            waveSpawner.doctorTrigger.TriggerEndingDialogue(1);
        else
            waveSpawner.doctorTrigger.TriggerEndingDialogue(0);
    }

    public void NextPage()
    {
        if (pageIndex + 1 >= journalEntrySprites.Length)
            return;

        pageIndex++;
        journalImage.sprite = journalEntrySprites[pageIndex];
    }

    public void PreviousPage()
    {
        if (pageIndex - 1 < 0)
            return;

        pageIndex--;
        journalImage.sprite = journalEntrySprites[pageIndex];
    }

    public void SetPage(int index)
    {
        pageIndex = index;
        journalImage.sprite = journalEntrySprites[index];
    }

}
