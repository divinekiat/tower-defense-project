﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewJournalUI : MonoBehaviour {

    public static NewJournalUI instance;

    void Awake()
    {
        // PlayerPrefs.DeleteAll(); // for debugging purposes
        if (instance != null)
        {
            Debug.LogError("More than one NewJournalUI in scene!");
            return;
        }
        instance = this;
    }

    public Image journalImage;
    private WaveSpawner waveSpawner;
    public Button nextButton;
    public Button previousButton;

    public Sprite[] journalEntrySprites;
    private int pageIndex;

    [HideInInspector]
    public bool afterClosed = false;

    void Start () {
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
        pageIndex = 0;
        SetPage(pageIndex);
    }
    
    void Update()
    {
        if (pageIndex + 1 > waveSpawner.lastJournalPage)
            nextButton.interactable = false;
        else
            nextButton.interactable = true;

        if (pageIndex - 1 < 0)
            previousButton.interactable = false;
        else
            previousButton.interactable = true;
    }

    public void OpenJournalUI()
    {
        GetComponent<Canvas>().enabled = true;
    }

    public void CloseJournalUI()
    {
        Time.timeScale = FastForward.gameSpeedScale;
        afterClosed = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void NextPage()
    {
        if (pageIndex + 1 >= journalEntrySprites.Length)
            return;

        pageIndex++;
        journalImage.sprite = journalEntrySprites[pageIndex];
    }

    public void PreviousPage()
    {
        if (pageIndex - 1 < 0)
            return;

        pageIndex--;
        journalImage.sprite = journalEntrySprites[pageIndex];
    }

    public void SetPage(int index)
    {
        pageIndex = index;
        journalImage.sprite = journalEntrySprites[index];
    }

}
