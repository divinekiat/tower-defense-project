﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCanvasButtonHandler : MonoBehaviour {

    public Button pauseButton;
    public Button fastForwardButton;
    public Button journalButton;
    public AudioSource buttonClicker;

    private NewJournalUI newJournalUI;
    private FastForward fastForward;
    private PauseMenu pauseMenu;

    void Start () {
        newJournalUI = GameObject.Find("NewJournalUI").GetComponent<NewJournalUI>();
        fastForward = GameObject.Find("GameMaster").GetComponent<FastForward>();
        pauseMenu = GameObject.Find("GameMaster").GetComponent<PauseMenu>();

        pauseButton.onClick.AddListener(PauseMenu);
        fastForwardButton.onClick.AddListener(FastForward);
        journalButton.onClick.AddListener(JournalButton);
    }
	
	void PauseMenu()
    {
        buttonClicker.Play();
        pauseMenu.Toggle();
    }

    void FastForward()
    {
        buttonClicker.Play();
        fastForward.SpeedUp();
    }

    void JournalButton()
    {
        buttonClicker.Play();
        newJournalUI.OpenJournalUI();
    }
}
