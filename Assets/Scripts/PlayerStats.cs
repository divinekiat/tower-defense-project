﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public static int Resources; // Immu
    public int startResources = 2000;

    public static int Money = 0; // Beatcoins

    public int correctAnswerReward = 0;

    // public static int Lives;
    // public int startLives = 2;

    public static int Rounds;

    public int addResourcesForEasy = 500;

    void Start()
    {
        Resources = startResources;
        // Lives = startLives;

        if(PlayerPrefs.GetInt("difficulty", 0) == 0)
        {
            Resources += addResourcesForEasy;
        }

        Rounds = 0;
    }
}
