﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class QuizManager : MonoBehaviour {

    public static QuizManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one QuizManager in scene!");
            return;
        }
        instance = this;
    }

    public bool isTimed = false;

    public GameObject MultipleChoicePanel;
    public GameObject TrueOrFalsePanel;

    public AudioClip backgroundMusic;

    public Question[] questions;

    private Question currentQuestion;

    [SerializeField]
    private Text questionText;
    [SerializeField]
    private Text AText;
    [SerializeField]
    private Text BText;
    [SerializeField]
    private Text CText;
    [SerializeField]
    private Text DText;

    [SerializeField]
    private float questionTimeLimit = 5f;
    public Image timerBar;

    [HideInInspector]
    public bool questionIsAnswered = false;

    private DoctorDialogueTrigger doctorTrigger;
    private Coroutine questionTimeCO;
    private AudioSource backgroundAudioSource;
    private WaveSpawner waveSpawner;

    void Start()
    {
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
        doctorTrigger = FindObjectOfType<DoctorDialogueTrigger>();
        backgroundAudioSource = GameObject.Find("GameMaster").GetComponent<AudioSource>();

        if (!isTimed)
            timerBar.gameObject.SetActive(false);
    }

    void Update()
    {
        if (questionIsAnswered)
            GetComponent<Canvas>().enabled = false;
    }

    public void ShowQuestion()
    {
        waveSpawner.screenLocker.enabled = true;
        if(backgroundAudioSource != null)
        {
            backgroundAudioSource.clip = backgroundMusic;
            backgroundAudioSource.Play();
        }

        GetComponent<Canvas>().enabled = true;
        SetCurrentQuestion();
        if(isTimed)
        {
            Time.timeScale = 0f;
            questionTimeCO = StartCoroutine(QuestionTime());
        }
    }

    void SetCurrentQuestion()
    {
        // currentQuestion = questions[doctorTrigger.currentFactIndex];
        currentQuestion = questions[waveSpawner.waveIndex - 1]; // -1 because index is added right after the wave finishes
        questionIsAnswered = false;
        questionText.text = currentQuestion.question;
        if(currentQuestion.questionType == Question.QuestionType.MultipleChoice)
        {
            AText.text = "A. " + currentQuestion.choices[0];
            BText.text = "B. " + currentQuestion.choices[1];
            CText.text = "C. " + currentQuestion.choices[2];
            DText.text = "D. " + currentQuestion.choices[3];
            MultipleChoicePanel.SetActive(true);
            TrueOrFalsePanel.SetActive(false);
        } else
        {
            TrueOrFalsePanel.SetActive(true);
            MultipleChoicePanel.SetActive(false);
        }
    }

    IEnumerator QuestionTime()
    {
        float timeLeft = questionTimeLimit;
        while(timeLeft > 0)
        {
            timeLeft -= Time.unscaledDeltaTime;
            timerBar.fillAmount = timeLeft / questionTimeLimit;
            yield return null;
        }
        // yield return new WaitForSecondsRealtime(questionTimeLimit);

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
        doctorTrigger.TimesUp();
    }

    public void UserSelectA()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.answer == 'A')
        { 
            doctorTrigger.CorrectAnswer();
        } else
        {
            doctorTrigger.WrongAnswer();
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void UserSelectB()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.answer == 'B')
        {
            doctorTrigger.CorrectAnswer();
        }
        else
        {
            doctorTrigger.WrongAnswer();;
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void UserSelectC()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.answer == 'C')
        {
            doctorTrigger.CorrectAnswer();
        }
        else
        {
            doctorTrigger.WrongAnswer();
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void UserSelectD()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.answer == 'D')
        {
            doctorTrigger.CorrectAnswer();
        }
        else
        {
            doctorTrigger.WrongAnswer();
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void UserSelectTrue()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.TOrFAnswer == true)
        {
            doctorTrigger.CorrectAnswer();
        }
        else
        {
            doctorTrigger.WrongAnswer();
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }

    public void UserSelectFalse()
    {
        if (isTimed)
            StopCoroutine(questionTimeCO);
        if (currentQuestion.TOrFAnswer == false)
        {
            doctorTrigger.CorrectAnswer();
        }
        else
        {
            doctorTrigger.WrongAnswer();
        }

        Time.timeScale = 1f;
        questionIsAnswered = true;
        GetComponent<Canvas>().enabled = false;
    }
}
