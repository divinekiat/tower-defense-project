﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Question {

    public enum QuestionType { MultipleChoice, TrueOrFalse}

    public QuestionType questionType;

    [TextArea]
    public string question;
    public char answer; // A, B, C, or D
    public bool TOrFAnswer;

    // A = choices[0], B = choices[1], ...
    public string[] choices;
    public SpriteSentence[] fact; // for the doctor dialog

    public SpriteSentence[] wrongResponse;
    public SpriteSentence[] correctResponse;
    public SpriteSentence[] timesUpResponse;

}
