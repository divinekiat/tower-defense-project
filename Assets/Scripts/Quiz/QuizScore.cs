﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizScore : MonoBehaviour {

    private WaveSpawner waveSpawner;
    public Text quizScoreText;

    void Awake()
    {
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
    }

    private void OnEnable()
    {
        quizScoreText.text = DoctorDialogueTrigger.scoreInLevel + "/" + waveSpawner.waveIndex;
    }
}
