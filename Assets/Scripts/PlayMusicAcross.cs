﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMusicAcross : MonoBehaviour {

    string sceneName;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        sceneName = SceneManager.GetActiveScene().name;
        if (!(sceneName == "MainMenu" ||
            sceneName == "LevelSelect" ||
            sceneName.Contains("StageSelect")))
            Destroy(gameObject);
    }
}
