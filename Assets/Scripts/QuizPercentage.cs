﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizPercentage : MonoBehaviour
{

    public Text percentageText;
    public Text totalQuizScoreText;
    private GameManager gameManager;

    private int currentAlcoholScore;

    private void Awake()
    {
        gameManager = GameObject.Find("GameMaster").GetComponent<GameManager>();
    }

    void OnEnable()
    {
        currentAlcoholScore = PlayerPrefs.GetInt("AlcoholScore");
        StartCoroutine(AnimateText());
        totalQuizScoreText.text = currentAlcoholScore + "/" + gameManager.totalQuizHPS;
    }

    IEnumerator AnimateText()
    {
        percentageText.text = "0%";
        float percentage = 0;

        yield return new WaitForSeconds(.7f);
        
        float quizPercentage = 100f * currentAlcoholScore / gameManager.totalQuizHPS;

        while (percentage < quizPercentage)
        {
            percentage += 0.1f;
            percentageText.text = percentage.ToString("F1") + "%";

            yield return new WaitForSeconds(.05f);
        }

    }

}
