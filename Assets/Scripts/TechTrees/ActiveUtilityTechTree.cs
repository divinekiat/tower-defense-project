﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ActiveUtilityTechTree : TechTree
{
    [HideInInspector]
    public TurretStats[] A, B, C;

    public Button AUpgradeButton;
    public Text AUpgradeName;
    public Text AUpgradeCost;

    public Button BUpgradeButton;
    public Text BUpgradeName;
    public Text BUpgradeCost;

    public Button CUpgradeButton;
    public Text CUpgradeName;
    public Text CUpgradeCost;

    public Button upgradeButton;
    public Text upgradeCounter;
    public Text upgradeText;

    public GameObject linesButtons;
    public GameObject activate;

    public Image cooldownImage;
    public Button activateButton;

    private TurretStats upgradeTS;
    private TurretStats nextTS;

    private UtilityTurret turret;

    public override void SetTarget(Node _target)
    {
        target = _target;

        A = target.A;
        B = target.B;
        C = target.C;

        turret = target.turret.GetComponent<UtilityTurret>();

        if (!target.isActiveAbilityChosen)
        {
            AUpgradeName.text = A[target.AIndex].name;
            AUpgradeCost.text = A[target.AIndex].cost.ToString();

            BUpgradeName.text = B[target.BIndex].name;
            BUpgradeCost.text = B[target.BIndex].cost.ToString();

            CUpgradeName.text = C[target.CIndex].name;
            CUpgradeCost.text = C[target.CIndex].cost.ToString();

            linesButtons.SetActive(true);
            activate.SetActive(false);

            upgradeText.text = "Upgrade";
            upgradeCounter.text = "";

            upgradeButton.enabled = true;
            upgradeButton.interactable = true;
        }
        else
        {
            linesButtons.SetActive(false);
            activate.SetActive(true);

            if (target.isAMaxed || target.isBMaxed || target.isCMaxed)
            {
                upgradeButton.enabled = false;
                upgradeButton.interactable = false;
                upgradeCounter.text = "Maxed";
                upgradeText.text = "Upgrade";
            }

            if (target.ActiveAbilityLineChosen.Equals("A"))
            {
                if(!target.isAMaxed)
                {
                    upgradeCounter.text = "Cost: " + A[target.AIndex].cost;
                    upgradeText.text = "Upgrade to " + A[target.AIndex].name;
                }
                
            }
            else if (target.ActiveAbilityLineChosen.Equals("B"))
            {
                if (!target.isBMaxed)
                {
                    upgradeCounter.text = "Cost: " + B[target.BIndex].cost;
                    upgradeText.text = "Upgrade to " + B[target.BIndex].name;
                }
            }
            else if (target.ActiveAbilityLineChosen.Equals("C"))
            {
                if (!target.isCMaxed)
                {
                    upgradeCounter.text = "Cost: " + C[target.CIndex].cost;
                    upgradeText.text = "Upgrade to " + C[target.CIndex].name;
                }
            }
        }

        sellAmount.text = target.GetSellAmount().ToString();

        ui.SetActive(true);
        closeButton.SetActive(true);
    }

    void Update()
    {
        if (turret.cooldownFillAmount >= 1)
        {
            activateButton.interactable = true;
            activateButton.enabled = true;
        } else
        {
            activateButton.interactable = false;
            activateButton.enabled = false;
        }

        cooldownImage.fillAmount = turret.cooldownFillAmount;
    }

    public void Activate()
    {
        turret.ActiveAffectArea();
    }

    public void UpgradeA()
    {
        target.ActiveAbilityLineChosen = "A";
        upgradeTS = A[target.AIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        nextTS = A[target.AIndex + 1];
    }

    public void UpgradeB()
    {
        target.ActiveAbilityLineChosen = "B";
        upgradeTS = B[target.BIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        nextTS = B[target.BIndex + 1];
    }

    public void UpgradeC()
    {
        target.ActiveAbilityLineChosen = "C";
        upgradeTS = C[target.CIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        nextTS = C[target.CIndex + 1];
    }

    public void Upgrade()
    {
        if (upgradeTS == null && !target.isActiveAbilityChosen)
        {
            buildManager.FlashTextDisplay("No selected upgrade", 1);
            return;
        }

        if (!target.isActiveAbilityChosen)
        {
            target.isActiveAbilityChosen = true;
            linesButtons.SetActive(false);
            activate.SetActive(true);

            if (upgradeTS == A[target.AIndex])
            {
                target.AIndex++;
            }
            else if (upgradeTS == B[target.BIndex])
            {
                target.BIndex++;
            }
            else if (upgradeTS == C[target.CIndex])
            {
                target.CIndex++;
            }

        } else
        {
            if (target.ActiveAbilityLineChosen.Equals("A"))
            {
                upgradeTS = A[target.AIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);
                
                if (target.AIndex + 1 < A.Length)
                    nextTS = A[++target.AIndex];
                else
                {
                    target.isAMaxed = true;
                    nextTS = null;
                }
            }
            else if (target.ActiveAbilityLineChosen.Equals("B"))
            {
                upgradeTS = B[target.BIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);
               
                if (target.BIndex + 1 < B.Length)
                    nextTS = B[++target.BIndex];
                else
                {
                    target.isBMaxed = true;
                    nextTS = null;
                }
            }
            else if (target.ActiveAbilityLineChosen.Equals("C"))
            {
                upgradeTS = C[target.CIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);
               
                if (target.CIndex + 1 < C.Length)
                    nextTS = C[++target.CIndex];
                else
                {
                    target.isCMaxed = true;
                    nextTS = null;
                }
            }
        }

        target.UpgradeTurret(upgradeTS, nextTS);
        upgradeTS = null;

        BuildManager.instance.DeselectNode();
    }
}
