﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PassiveUtilityTechTree : TechTree
{
    [HideInInspector]
    public TurretStats[] A, B, C;

    public Button AUpgradeButton;
    public Text AUpgradeName;
    public Text AUpgradeCost;

    public Button BUpgradeButton;
    public Text BUpgradeName;
    public Text BUpgradeCost;

    public Button CUpgradeButton;
    public Text CUpgradeName;
    public Text CUpgradeCost;

    private TurretStats upgradeTS;
    private TurretStats nextTS;

    public GameObject linesButtons;
    public Text upgradeText;
    public Text upgradeCounter;
    public Button upgradeButton;

    // private UtilityTurret turret;

    public override void SetTarget(Node _target)
    {
        target = _target;

        A = target.A;
        B = target.B;
        C = target.C;

        // turret = target.turret.GetComponent<UtilityTurret>();

        if (!target.isUpgraded)
        {
            AUpgradeName.text = A[target.AIndex].name;
            AUpgradeCost.text = A[target.AIndex].cost.ToString();

            BUpgradeName.text = B[target.BIndex].name;
            BUpgradeCost.text = B[target.BIndex].cost.ToString();

            CUpgradeName.text = C[target.CIndex].name;
            CUpgradeCost.text = C[target.CIndex].cost.ToString();

            linesButtons.SetActive(true);

            upgradeText.text = "Upgrade";
            upgradeCounter.text = "";
            upgradeButton.enabled = true;
            upgradeButton.interactable = true;
        }
        else
        {
            linesButtons.SetActive(false);

            if (target.isAMaxed || target.isBMaxed || target.isCMaxed)
            {
                upgradeButton.enabled = false;
                upgradeButton.interactable = false;
                upgradeCounter.text = "Maxed";
                upgradeText.text = "Upgrade";
            }
            else
            {
                upgradeButton.enabled = true;
                upgradeButton.interactable = true;
                upgradeCounter.text = "";
                upgradeText.text = "Upgrade";
            }

            if (target.turretLineChosen.Equals("A"))
            {
                if (!target.isAMaxed)
                {
                    upgradeText.text = "Upgrade to " + A[target.AIndex].name;
                    upgradeCounter.text = "Cost: " + A[target.AIndex].cost;
                }
            }

            if (target.turretLineChosen.Equals("B"))
            {
                if (!target.isBMaxed)
                {
                    upgradeText.text = "Upgrade to " + B[target.BIndex].name;
                    upgradeCounter.text = "Cost: " + B[target.BIndex].cost;
                }
            }

            if (target.turretLineChosen.Equals("C"))
            {
                if (!target.isCMaxed)
                {
                    upgradeText.text = "Upgrade to " + C[target.CIndex].name;
                    upgradeCounter.text = "Cost: " + C[target.CIndex].cost;
                }
            }
        }

        sellAmount.text = target.GetSellAmount().ToString();

        ui.SetActive(true);
        closeButton.SetActive(true);
    }

    public void UpgradeA()
    {
        target.turretLineChosen = "A";
        upgradeTS = A[target.AIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        if (target.AIndex + 1 < A.Length)
            nextTS = A[target.AIndex + 1];
        else
        {
            target.isAMaxed = true;
            nextTS = null;
        }
    }

    public void UpgradeB()
    {
        target.turretLineChosen = "B";
        upgradeTS = B[target.BIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        if (target.BIndex + 1 < B.Length)
            nextTS = B[target.BIndex + 1];
        else
        {
            target.isBMaxed = true;
            nextTS = null;
        }
    }

    public void UpgradeC()
    {
        target.turretLineChosen = "C";
        upgradeTS = C[target.CIndex];
        upgradeCounter.text = "to " + upgradeTS.name;
        buildManager.FlashTextDisplay(upgradeTS.name, 0);
        buildManager.FlashTextDisplay(upgradeTS.description, 1);
        if (target.CIndex + 1 < C.Length)
            nextTS = C[target.CIndex + 1];
        else
        {
            target.isCMaxed = true;
            nextTS = null;
        }
    }

    public void Upgrade()
    {
        if (upgradeTS == null && !target.isUpgraded)
        {
            buildManager.FlashTextDisplay("No selected upgrade", 1);
            return;
        }

        if (!target.isUpgraded)
        {
            target.isUpgraded = true;
            linesButtons.SetActive(false);

            if (upgradeTS == A[target.AIndex])
                target.AIndex++;

            if (upgradeTS == B[target.BIndex])
                target.BIndex++;

            if (upgradeTS == C[target.CIndex])
                target.CIndex++;
        }
        else
        {
            if (target.turretLineChosen.Equals("A"))
            {
                upgradeTS = A[target.AIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);

                if (target.AIndex + 1 < A.Length)
                    nextTS = A[++target.AIndex];
                else
                {
                    target.isAMaxed = true;
                    nextTS = null;
                }
            }
            else if (target.turretLineChosen.Equals("B"))
            {
                upgradeTS = B[target.BIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);

                if (target.BIndex + 1 < B.Length)
                    nextTS = B[++target.BIndex];
                else
                {
                    target.isBMaxed = true;
                    nextTS = null;
                }
            }
            else if (target.turretLineChosen.Equals("C"))
            {
                upgradeTS = C[target.CIndex];
                buildManager.FlashTextDisplay(upgradeTS.name, 0);
                buildManager.FlashTextDisplay(upgradeTS.description, 1);

                if (target.CIndex + 1 < C.Length)
                    nextTS = C[++target.CIndex];
                else
                {
                    target.isCMaxed = true;
                    nextTS = null;
                }
            }

        }

        target.UpgradeTurret(upgradeTS, nextTS);
        upgradeTS = null;
        BuildManager.instance.DeselectNode();
    }
}
