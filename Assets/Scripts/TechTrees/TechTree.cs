﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TechTree : MonoBehaviour
{
    public GameObject ui;
   
    protected Node target;

    public Text sellAmount;

    protected BuildManager buildManager;

    public GameObject closeButton;

    void Start()
    {
        buildManager = BuildManager.instance;
    }

    // TODO: Add button to close the TechTree

    public virtual void SetTarget(Node _target)
    {
        
    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void Sell()
    {
        target.SellTurret();
        BuildManager.instance.DeselectNode();
    }
}
