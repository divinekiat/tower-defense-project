﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Endpoint : MonoBehaviour {

    public static Endpoint instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one Endpoint in scene!");
            return;
        }
        instance = this;
    }

    [HideInInspector]
    public static float Health;
    public float startHealth = 100f;

    [Header("Unity Stuff")]
    public Image healthBar;

    public Material blendMat;

    private SpriteRenderer bgSpriteRenderer;

    void Start()
    {
        Health = startHealth;
        bgSpriteRenderer = GameObject.Find("BackgroundImage").GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        blendMat.SetFloat("_LerpValue", 1 - Health / startHealth);
    }

    public void TakeDamage(float amount)
    {
        if (GameManager.GameIsOver)
            return;

        Health -= amount;
        healthBar.fillAmount = Health / startHealth;

        bgSpriteRenderer.color =new Color(bgSpriteRenderer.color.r, 
                                          bgSpriteRenderer.color.g, 
                                          healthBar.fillAmount); // change the blue to make it yellowish
    }
}
