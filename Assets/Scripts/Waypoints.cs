﻿using UnityEngine;

public class Waypoints : MonoBehaviour
{

    public static Transform[] points;

    public Transform waypointsBack;
    public static Transform[] pointsBack;

    public Transform waypointsNextStage;
    public static Transform[] pointsNextStage;

    void Awake()
    {

        points = new Transform[transform.childCount];

        for (int i = 0; i < points.Length; i++)
        {
            points[i] = transform.GetChild(i);
        }

        pointsBack = new Transform[waypointsBack.childCount];

        for (int i = 0; i < pointsBack.Length; i++)
        {
            pointsBack[i] = waypointsBack.GetChild(i);
        }

        pointsNextStage = new Transform[waypointsNextStage.childCount];

        for (int i = 0; i < pointsNextStage.Length; i++)
        {
            pointsNextStage[i] = waypointsNextStage.GetChild(i);
        }
    }
}
