﻿using UnityEngine;
using UnityEngine.UI;

public class StageSelector : MonoBehaviour
{

    private SceneFader fader;

    public Button[] levelButtons;

    void Start()
    {
        fader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);

        for (int i = 1; i < levelButtons.Length; i++)
        {
            if (i + 1 > levelReached)
            {
                levelButtons[i].interactable = false;
                levelButtons[i].GetComponentInChildren<Text>().enabled = false;
                levelButtons[i].transform.Find("Image").gameObject.GetComponent<Image>().enabled = true;
            } else
            {
                levelButtons[i].GetComponentInChildren<Text>().enabled = true;
                levelButtons[i].transform.Find("Image").gameObject.GetComponent<Image>().enabled = false;
            }  
        }
    }

    public void Select(string levelName)
    {
        fader.FadeTo(levelName);
    }
}
