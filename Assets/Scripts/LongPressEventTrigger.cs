﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class LongPressEventTrigger : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [Tooltip("How long must pointer be down on this object to trigger a long press")]
    public float durationThreshold = 1.0f;

    public UnityEvent onLongPress = new UnityEvent();

    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;

    private Image sellImage;

#pragma warning disable CS0114 // Member hides inherited member; missing override keyword
    private void Start()
#pragma warning restore CS0114 // Member hides inherited member; missing override keyword
    {
        Debug.Log("SELL");
        sellImage = GetComponent<Image>();
        sellImage.sprite = Resources.Load<Sprite>("WhiteSquare");
        sellImage.type = Image.Type.Filled;
        sellImage.fillMethod = Image.FillMethod.Radial360;
        sellImage.fillOrigin = 2; // 0 = bottom, 1 = right, 2 = top, 3 = left

        
    }

    private void Update()
    {
        if (isPointerDown && !longPressTriggered)
        {
            sellImage.fillAmount = 1 - Time.time + timePressStarted;

            if (Time.time - timePressStarted > durationThreshold)
            {
                longPressTriggered = true;
                onLongPress.Invoke();
            }
        } else
        {
            sellImage.fillAmount = 1;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }
}