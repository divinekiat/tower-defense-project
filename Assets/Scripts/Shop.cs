﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    public TurretBlueprint STTurret;
    public TurretBlueprint AoETurret;
    public TurretBlueprint laserTurret;
    public TurretBlueprint activeUtilityTurret;
    public TurretBlueprint passiveUtilityTurret;

    public Text STTurretPrice;
    public Text AoETurretPrice;
    public Text laserTurretPrice;
    public Text activeUtilityTurretPrice;
    public Text passiveUtilityTurretPrice;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
        STTurretPrice.text = STTurret.root.cost.ToString();
        AoETurretPrice.text = AoETurret.root.cost.ToString();
        laserTurretPrice.text = laserTurret.root.cost.ToString();
        activeUtilityTurretPrice.text = activeUtilityTurret.root.cost.ToString();
        passiveUtilityTurretPrice.text = passiveUtilityTurret.root.cost.ToString();
}

    public void SelectSTTurret()
    {
        buildManager.SelectTurretToBuild(STTurret);
    }

    public void SelectAoETurret()
    {
        buildManager.SelectTurretToBuild(AoETurret);
    }

    public void SelectLaserTurret()
    {
        buildManager.SelectTurretToBuild(laserTurret);
    }

    public void SelectActiveUtilityTurret()
    {
        buildManager.SelectTurretToBuild(activeUtilityTurret);
    }

    public void SelectPassiveUtilityTurret()
    {
        buildManager.SelectTurretToBuild(passiveUtilityTurret);
    }
}
