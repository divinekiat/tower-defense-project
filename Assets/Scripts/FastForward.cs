﻿using UnityEngine;
using UnityEngine.UI;

public class FastForward : MonoBehaviour {

    public Text gameSpeedScaleText;
    public static float gameSpeedScale = 1f;

    void Start()
    {
        gameSpeedScale = 1f;
    }

    public void SpeedUp()
    {
        if (gameSpeedScale == 1f)
        {
            gameSpeedScale = 2f;
            gameSpeedScaleText.text = "2";
        }
        else if (gameSpeedScale == 2f)
        {
            gameSpeedScale = 4f;
            gameSpeedScaleText.text = "4";
        }
        else if (gameSpeedScale == 4f)
        {
            gameSpeedScale = 1f;
            gameSpeedScaleText.text = "1";
        }

        Time.timeScale = gameSpeedScale;
    }
}
