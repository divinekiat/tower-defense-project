﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoctorDialogueTrigger : MonoBehaviour {

    public string doctorName;
    public AudioClip correctSound;
    public AudioClip wrongSound;
    public AudioClip congratsSound;
    public AudioSource feedbackAudioSource;

    private Dialogue[] facts;
    private Dialogue[] correctResponses;
    private Dialogue[] wrongResponses;
    private Dialogue[] timesUpResponses;
    [HideInInspector]
    public int currentFactIndex;

    [HideInInspector]
    public bool isShown = false;

    public AudioClip introMusic;
    public Dialogue intro;

    public AudioClip endingMusic;
    public Dialogue[] ending; // 0 = bad, 1 = okay, 2 = good

    private WaveSpawner waveSpawner;

    public static int scoreInLevel = 0;

    private int quizNoAnswered;
    private int stageQuizScore;

    void Start()
    {
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
        scoreInLevel = 0;   // so it goes back to 0 after next scene

        facts = new Dialogue[QuizManager.instance.questions.Length];
        correctResponses = new Dialogue[QuizManager.instance.questions.Length];
        wrongResponses = new Dialogue[QuizManager.instance.questions.Length];
        timesUpResponses = new Dialogue[QuizManager.instance.questions.Length];

        for (int i = 0; i < facts.Length; i++)
        { 
            facts[i] = new Dialogue();
            facts[i].name = doctorName;
            facts[i].sentences = QuizManager.instance.questions[i].fact;

            correctResponses[i] = new Dialogue();
            correctResponses[i].name = doctorName;
            correctResponses[i].sentences = QuizManager.instance.questions[i].correctResponse;

            wrongResponses[i] = new Dialogue();
            wrongResponses[i].name = doctorName;
            wrongResponses[i].sentences = QuizManager.instance.questions[i].wrongResponse;

            timesUpResponses[i] = new Dialogue();
            timesUpResponses[i].name = doctorName;
            timesUpResponses[i].sentences = QuizManager.instance.questions[i].timesUpResponse;
        }
    }

    public void TriggerIntroDialogue()
    {
        isShown = true;

        FindObjectOfType<DialogueManager>().StartDialogue(intro);

        WaveSpawner.audioSource.clip = introMusic;
        WaveSpawner.audioSource.Play();
    }

    //  i = 0 = bad, 1 = okay, 2 = good
    public void TriggerEndingDialogue(int i)
    {
        // Play sound
        feedbackAudioSource.clip = congratsSound;
        feedbackAudioSource.Play();

        isShown = true;

        FindObjectOfType<DialogueManager>().StartDialogue(ending[i]);

        WaveSpawner.audioSource.clip = endingMusic;
        WaveSpawner.audioSource.Play();
    }

    public void TriggerDialogue()
    {
        isShown = true;
        // Dialogue dialogue = facts[currentFactIndex];

        Dialogue dialogue = facts[waveSpawner.waveIndex];


        quizNoAnswered = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered");

        if (!(waveSpawner.waveIndex < quizNoAnswered))
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

    // if the player gets correct in quiz
    public void CorrectAnswer()
    {
        // Play sound
        feedbackAudioSource.clip = correctSound;
        feedbackAudioSource.Play();

        // Add 1 to current score
        int currentAlcoholScore = PlayerPrefs.GetInt("AlcoholScore");
        currentAlcoholScore++;
        PlayerPrefs.SetInt("AlcoholScore", currentAlcoholScore);

        stageQuizScore = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizScore", 0);
        stageQuizScore++;
        PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "quizScore", stageQuizScore);

        if (waveSpawner.waveIndex > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered"))
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "quizNoAnswered", waveSpawner.waveIndex);
        Debug.Log("Last quiz answered: " + PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered"));

        scoreInLevel++;

        Debug.Log("Current score: " + PlayerPrefs.GetInt("AlcoholScore"));

        // Dialogue correctResponse = correctResponses[currentFactIndex]; 
        Dialogue correctResponse = correctResponses[waveSpawner.waveIndex - 1]; // -1 because index is added right after the wave finishes
        FindObjectOfType<DialogueManager>().StartDialogue(correctResponse);
        currentFactIndex++;

        PlayerStats playerStats = waveSpawner.gameObject.GetComponent<PlayerStats>();

        PlayerStats.Resources += playerStats.correctAnswerReward;
    }

    // if the player gets wrong in quiz
    public void WrongAnswer()
    {
        // Play sound
        feedbackAudioSource.clip = wrongSound;
        feedbackAudioSource.Play();

        if(waveSpawner.waveIndex > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered"))
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "quizNoAnswered", waveSpawner.waveIndex);
        Debug.Log("Last quiz answered: " + PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered"));

        // Dialogue wrongResponse = wrongResponses[currentFactIndex];
        Dialogue wrongResponse = wrongResponses[waveSpawner.waveIndex - 1]; // -1 because index is added right after the wave finishes
        FindObjectOfType<DialogueManager>().StartDialogue(wrongResponse);
        currentFactIndex++;

        Debug.Log("Current score: " + PlayerPrefs.GetInt("AlcoholScore"));
    }

    public void TimesUp()
    {
        // Dialogue timesUpResponse = timesUpResponses[currentFactIndex];
        Dialogue timesUpResponse = timesUpResponses[waveSpawner.waveIndex - 1]; // -1 because index is added right after the wave finishes
        FindObjectOfType<DialogueManager>().StartDialogue(timesUpResponse);
        currentFactIndex++;
    }
}
