﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public bool isTutorial = false;

    public Text nameText;
    public Text dialogueText;
    public Image image;
    public Sprite defaultSprite;

    private GameManager gameManager;
    private PlayerStats playerStats;

    public Animator animator;

    private WaveSpawner waveSpawner;

    private Queue<string> sentences;
    private Queue<Sprite> sprites;

    private bool introJournalIsShown;
    private bool endingJournalIsShown;

    private bool isTypingSentence;
    private IEnumerator typeSentenceCoroutine;
    private string sentence;

    public GameObject continueButton;
    public GameObject standardButton;
    public GameObject challengeButton;
    public GameObject absolutelyButton;
    public GameObject noThanksButton;

    public Button laserButton;
    public Button activeTowerButton;
    public Button sellButton;
    public Button upgradeButton;
    public Button laserLineAButton;

    public LongPressEventTrigger lpetToDisable;

    public GameObject[] gameObjectsToDisableDuringTutorial;
    public Image imageToDisableDuringTutorial;

    public Node nodeToEnable;

    public GameObject selectingArrow;
    public GameObject buildingTurretArrow;
    public GameObject upgradingArrow;
    public GameObject selectingTowerArrow;
    public GameObject buildingTowerArrow;   

    public Shop shop;
    public GameObject laserTechTree;

    private bool sentenceStartsWithDifficultySetting = false;
    private bool sentenceStartsWithTutorialSetting = false;
    private bool tutorialSettingDone = false;
    private bool tutorialSelecting = false;
    private bool tutorialSelectingDone = false;
    private bool tutorialBuildingTurret = false;
    private bool tutorialBuildingTurretDone = false;
    private bool tutorialTechTree = false;
    private bool tutorialTechTreeDone = false;
    private bool tutorialUpgrading = false;
    private bool tutorialUpgradingDone = false;
    private bool tutorialBuildingTower = false;
    private bool tutorialBuildingTowerDone = false;
    private bool tutorialSelling = false;
    private bool tutorialSellingDone = false;

    private bool goToTutorial = false;
    private bool goToTutorialYesDone = false;
    private bool fadingToTutorial = false;

    private bool upgradeCanBeEnabled = false;
    private bool canSell = false;

    void Awake() { 
        waveSpawner = GameObject.Find("GameMaster").GetComponent<WaveSpawner>();
        gameManager = GameObject.Find("GameMaster").GetComponent<GameManager>();
        playerStats = GameObject.Find("GameMaster").GetComponent<PlayerStats>();
        sentences = new Queue<string>();
        sprites = new Queue<Sprite>();
        introJournalIsShown = false;
        endingJournalIsShown = false;
        isTypingSentence = false;

        sentenceStartsWithDifficultySetting = false;
        sentenceStartsWithTutorialSetting = false;
        tutorialSettingDone = false;
        tutorialSelecting = false;
        tutorialSelectingDone = false;
        tutorialBuildingTurret = false;
        tutorialBuildingTurretDone = false;
        tutorialTechTree = false;
        tutorialTechTreeDone = false;
        tutorialUpgrading = false;
        tutorialUpgradingDone = false;
        tutorialBuildingTower = false;
        tutorialBuildingTowerDone = false;
        tutorialSelling = false;
        tutorialSellingDone = false;

        goToTutorialYesDone = false;
        fadingToTutorial = false;
    }

    private void Update()
    {
        if (!isTutorial)
            return;

        if (tutorialSelectingDone && !tutorialBuildingTurretDone)
        {
            shop.SelectLaserTurret();
        }

        if (!tutorialBuildingTurretDone
            && GameObject.Find("Laser_0(Clone)"))
        {
            TurretBuilt();
        }

        if (laserTechTree.activeSelf
            && !tutorialTechTreeDone)
        {
            Debug.Log("Opened tech tree");
            OpenedTechTree();
        }

        if(laserTechTree.activeSelf)
        {
            upgradeButton.interactable = false;
        }

        if(upgradeCanBeEnabled)
        {
            upgradeButton.interactable = true;
        }

        if (!tutorialBuildingTowerDone
            && GameObject.Find("Utility_Active_0(Clone)"))
        {
            TowerBuilt();
        }

        if(canSell)
        {
            lpetToDisable.enabled = true;
        } else
        {
            lpetToDisable.enabled = false;
        }

        if (tutorialUpgradingDone 
            && laserTechTree.activeSelf
            && !tutorialSellingDone)
        {
            Selling2();
        }

    }

    public void StartDialogue(Dialogue dialogue)
    {
        waveSpawner.screenLocker.enabled = true;
        animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;
        
        sentences.Clear();
        sprites.Clear();

        foreach (SpriteSentence sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence.sentence);
            sprites.Enqueue(sentence.sprite);
        }
        
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (isTypingSentence)
        {
            StopCoroutine(typeSentenceCoroutine);
            
            if (sentence.StartsWith("["))
            {
                sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            }

            if(sentence.StartsWith("[DifficultySetting]") || sentenceStartsWithDifficultySetting)
            {
                continueButton.SetActive(false);
                standardButton.SetActive(true);
                challengeButton.SetActive(true);
                sentenceStartsWithDifficultySetting = false;
            }

            if (sentence.StartsWith("[TutorialSetting]") || sentenceStartsWithTutorialSetting)
            {
                continueButton.SetActive(false);
                absolutelyButton.SetActive(true);
                noThanksButton.SetActive(true);
                sentenceStartsWithTutorialSetting = false;
            }

            dialogueText.text = sentence;   

            isTypingSentence = false;
            return;
        } else
        {
            if(goToTutorialYesDone)
            {
                PlayerPrefs.SetString("previousScene", "Alcohol01");
                GameObject.Find("SceneFader").GetComponent<SceneFader>().FadeTo("AlcoholTutorial");
                fadingToTutorial = true;
            }
        }

        if(tutorialSelecting && !tutorialSelectingDone)
        {
            Selecting();
            tutorialSelecting = false;
            return;
        }

        if (tutorialBuildingTurret && !tutorialBuildingTurretDone)
        {
            BuildingTurret();
            tutorialBuildingTurret = false;
            return;
        }

        if (tutorialTechTree && !tutorialTechTreeDone)
        {
            OpeningTechTree();
            tutorialTechTree = false;
            return;
        }

        if (tutorialUpgrading && !tutorialUpgradingDone)
        {
            Upgrading();
            tutorialUpgrading = false;
            return;
        }

        if (tutorialBuildingTower && !tutorialBuildingTowerDone)
        {
            SelectingTower();
            tutorialBuildingTower = false;
            return;
        }

        if (tutorialSelling && !tutorialSellingDone)
        {
            Selling();
            tutorialSelling = false;
            return;
        }

        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        
        sentence = sentences.Dequeue();

        if(PlayerPrefs.GetString("previousScene") == "tutorial")
        {
            goToTutorial = true;
            Debug.Log("previous scene is tutorial");
            while(!sentence.StartsWith("[WelcomeBack]"))
            {
                sentence = sentences.Dequeue();
            } 

            PlayerPrefs.SetString("previousScene", "notTutorial");
        }

        Sprite sprite = sprites.Dequeue();
        if (sprite != null)
            image.sprite = sprite;
        else
            image.sprite = defaultSprite;

        if (string.IsNullOrEmpty(sentence))
        {
            Debug.LogWarning("Empty dialogue string. Modify in Inspector");
            sentence = "Empty dialogue string. Modify in Inspector";
        }
       
        StopAllCoroutines();
        typeSentenceCoroutine = TypeSentence(sentence);
        StartCoroutine(typeSentenceCoroutine);
    }

    IEnumerator TypeSentence(string sentence)
    {
        if (fadingToTutorial)
            yield break;

        if(sentence.StartsWith("[TutorialEnd]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            if (PlayerPrefs.GetString("previousScene") == "mainMenu")
                sentence = "Great! Now you know the basics, we can go back to the title screen";
        }

        if (!tutorialSettingDone && goToTutorial)
        {
            if(sentence.StartsWith("[TutorialYes]"))
            {
                sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
                goToTutorialYesDone = true;
            }

            if(sentence.StartsWith("[TutorialNo]"))
            {
                DisplayNextSentence();
                yield return null;
            }
        }

        if (!tutorialSettingDone && !goToTutorial)
        {
            if (sentence.StartsWith("[TutorialYes]") || sentence.StartsWith("[WelcomeBack]"))
            {
                DisplayNextSentence();
                yield return null;
            }
            if (sentence.StartsWith("[TutorialNo]"))
            {
                sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            }
        }

        isTypingSentence = true;
        dialogueText.text = "";

        if (sentence.StartsWith("[DifficultySetting]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            sentenceStartsWithDifficultySetting = true;
        }

        if (sentence.StartsWith("[TutorialSetting]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            sentenceStartsWithTutorialSetting = true;
        }

        if (sentence.StartsWith("[WelcomeBack]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
        }

        if (sentence.StartsWith("[Selecting]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialSelecting = true;
        }

        if (sentence.StartsWith("[BuildingTurret]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialBuildingTurret = true;
        }

        if (sentence.StartsWith("[OpeningTechTree]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialTechTree = true;
        }

        if (sentence.StartsWith("[Upgrading]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialUpgrading = true;
        }

        if (sentence.StartsWith("[BuildingTower]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialBuildingTower = true;
        }

        if (sentence.StartsWith("[Selling]"))
        {
            sentence = sentence.Substring(sentence.IndexOf(']') + 1, sentence.Length - sentence.IndexOf(']') - 1);
            tutorialSelling = true;
        }

        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }

        if (sentence.StartsWith("[DifficultySetting]") || sentenceStartsWithDifficultySetting)
        {
            continueButton.SetActive(false);
            standardButton.SetActive(true);
            challengeButton.SetActive(true);
            sentenceStartsWithDifficultySetting = false;
        }

        if (sentence.StartsWith("[TutorialSetting]") || sentenceStartsWithTutorialSetting)
        {
            continueButton.SetActive(false);
            absolutelyButton.SetActive(true);
            noThanksButton.SetActive(true);
            sentenceStartsWithTutorialSetting = false;
        }

        isTypingSentence = false;
    }

    public void DisableObjects()
    {
        for(int i = 0; i < gameObjectsToDisableDuringTutorial.Length; i++)
        {
            gameObjectsToDisableDuringTutorial[i].SetActive(false);
        }

        waveSpawner.screenLocker.enabled = false;
        imageToDisableDuringTutorial.enabled = false;
    }

    public void EnableObjects()
    {
        for (int i = 0; i < gameObjectsToDisableDuringTutorial.Length; i++)
        {
            gameObjectsToDisableDuringTutorial[i].SetActive(true);
        }

        waveSpawner.screenLocker.enabled = true;
        imageToDisableDuringTutorial.enabled = true;
    }

    public void Selecting()
    {
        DisableObjects();
        selectingArrow.SetActive(true);
        Debug.Log("Selecting");
    }

    public void SelectTap()
    {
        if(!tutorialSelectingDone)
        {
            Debug.Log("Click");
            EnableObjects();
            selectingArrow.SetActive(false);
            DisplayNextSentence();
            tutorialSelectingDone = true;
        }
    }

    public void BuildingTurret()
    {
        DisableObjects();
        buildingTurretArrow.SetActive(true);
        Debug.Log("Building Turret");
    }

    public void TurretBuilt()
    {
        EnableObjects();
        buildingTurretArrow.SetActive(false);
        tutorialBuildingTurretDone = true;
        DisplayNextSentence();
    }

    public void Upgrading()
    {
        DisableObjects();
        upgradingArrow.SetActive(true);
        upgradeButton.interactable = false;
        Debug.Log("Upgrading");
    }

    public void Upgraded()
    {
        EnableObjects();
        upgradeButton.interactable = false;
        laserLineAButton.interactable = false;
        upgradingArrow.SetActive(false);
        tutorialUpgradingDone = true;
        upgradeCanBeEnabled = false;
        DisplayNextSentence();
    }

    public void SelectingTower()
    {
        DisableObjects();
        activeTowerButton.interactable = true;
        laserButton.interactable = false;
        selectingTowerArrow.SetActive(true);
        Debug.Log("Selecting Tower");
    }

    public void BuildingTower()
    {
        nodeToEnable.enabled = true;
        selectingTowerArrow.SetActive(false);
        buildingTowerArrow.SetActive(true);
    }

    public void TowerBuilt()
    {
        EnableObjects();
        buildingTowerArrow.SetActive(false);
        tutorialBuildingTowerDone = true;
        DisplayNextSentence();
    }

    public void Selling()
    {
        DisableObjects();
        nodeToEnable.enabled = false;
        sellButton.interactable = true;
        laserButton.interactable = false;
        activeTowerButton.interactable = false;
        canSell = true;
        buildingTurretArrow.SetActive(true);
    }

    public void Selling2()
    {
        selectingTowerArrow.SetActive(true);
        buildingTurretArrow.SetActive(false);
    }
    
    public void Sold()
    {
        EnableObjects();
        selectingTowerArrow.SetActive(false);
        tutorialSellingDone = true;
        DisplayNextSentence();
        canSell = false;
    }

    public void MoveUpgradeArrowDown()
    {
        laserLineAButton.interactable = false;
        upgradeButton.interactable = true;
        upgradingArrow.transform.Translate(0, -105, 0);
        upgradeCanBeEnabled = true;
    }

    public void OpeningTechTree()
    {
        upgradeButton.interactable = false;
        DisableObjects();
        buildingTurretArrow.SetActive(true);
    }

    public void OpenedTechTree()
    {
        EnableObjects();
        buildingTurretArrow.SetActive(false);
        tutorialTechTreeDone = true;
        DisplayNextSentence();
    }

    public void StandardDifficulty()
    {
        PlayerPrefs.SetInt("difficulty", 0); // 0 = standard
        continueButton.SetActive(true);
        standardButton.SetActive(false);
        challengeButton.SetActive(false);
        DisplayNextSentence();
    }

    public void ChallengeDifficulty()
    {
        PlayerPrefs.SetInt("difficulty", 1);
        PlayerStats.Resources -= playerStats.addResourcesForEasy;
        continueButton.SetActive(true);
        standardButton.SetActive(false);
        challengeButton.SetActive(false);
        DisplayNextSentence();
    }

    public void TutorialYes()
    {
        goToTutorial = true;
        DisplayNextSentence();
        continueButton.SetActive(true);
        absolutelyButton.SetActive(false);
        noThanksButton.SetActive(false);
        sentenceStartsWithTutorialSetting = false;
        
    }

    public void TutorialNo()
    {
        goToTutorial = false;
        DisplayNextSentence();
        continueButton.SetActive(true);
        absolutelyButton.SetActive(false);
        noThanksButton.SetActive(false);
        sentenceStartsWithTutorialSetting = false;
    }

    void EndDialogue()
    {
        if(isTutorial)
        {
            PlayerPrefs.SetInt("tutorialIsDone", 1);

            if (PlayerPrefs.GetString("previousScene") == "mainMenu")
            {
                PlayerPrefs.SetString("previousScene", "tutorial");
                GameObject.Find("SceneFader").GetComponent<SceneFader>().FadeTo("MainMenu");
            } else
            {
                PlayerPrefs.SetString("previousScene", "tutorial");
                GameObject.Find("SceneFader").GetComponent<SceneFader>().FadeTo("Alcohol01");
            }

            
        }

        animator.SetBool("IsOpen", false);
        if (waveSpawner.waveIndex == 0 
            && !introJournalIsShown
            && waveSpawner.showIntro)
        {
            NewJournalUI.instance.OpenJournalUI();
            NewJournalUI.instance.SetPage(waveSpawner.lastJournalPage);
            introJournalIsShown = true;
            waveSpawner.doctorTrigger.isShown = false;
        }

        if (waveSpawner.waveIndex == waveSpawner.waves.Length 
            && WaveSpawner.EnemiesAlive <= 0)
        {
            if (gameManager.isLastLevel
               && !endingJournalIsShown)
            {
                FinalJournalUI.instance.OpenJournalUI();
                endingJournalIsShown = true;
            } else if (gameManager.isLastLevel)
            {
                gameManager.WinMode();
            } else
            {
                gameManager.WinLevel();
            }  
            waveSpawner.enabled = false;
        }

        waveSpawner.screenLocker.enabled = false;
    }
}
