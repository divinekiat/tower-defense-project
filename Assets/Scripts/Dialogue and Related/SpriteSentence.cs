﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpriteSentence {

    [TextArea]
    public string sentence;
    public Sprite sprite;
}
