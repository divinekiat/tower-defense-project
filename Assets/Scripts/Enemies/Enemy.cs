﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public enum Stage
    {
        one,
        two,
        three
    }

    public enum Mode
    {
        alcohol,
        meth,
        nicotine
    }

    public Stage stage;
    public Mode mode;

    public GameObject shieldToDestroy;

    public float startSpeed = 10f;

    [HideInInspector]
    public float speed;
    [HideInInspector]
    public float slownSpeed;

    public float startHealth = 100;
    private float health;

    public float damagePerSecond = 10;

    public int worth = 50;

    public GameObject deathEffect;
    public GameObject shieldDestroyEffect;

    [Header("Unity Stuff")]
    public Image healthBar;
    public Transform healthCanvas;

    private bool isDead = false;

    private EnemyMovement em;

    void Awake()
    {
        em = GetComponent<EnemyMovement>();
        speed = startSpeed;
        slownSpeed = speed;
        health = startHealth;
    }

    public void TakeDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    public void Slow(float pct)
    {
        speed = startSpeed * (1f - pct/100);
        slownSpeed = speed;
    }

    public void MoreSlow(float pct)
    {
        speed = slownSpeed * (1f - pct / 100);
    }

    public void Freeze()
    {
        if(!isDead)
            speed = 0;
    }

    void Die()
    {
        isDead = true;

        PlayerStats.Money += worth;

        WaveSpawner.EnemiesAlive--;

        if (mode.Equals(Mode.alcohol) && !stage.Equals(Stage.three)) {
            GameObject effect = (GameObject)Instantiate(shieldDestroyEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            Destroy(shieldToDestroy);
            em.GoToNextStage();
        }            
        else
        {
            GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
            Destroy(gameObject);
        }
            
    }

    public IEnumerator Damage(Endpoint endpoint, int time)
    {
        // how many seconds damage will take place
        while (time > 0)
        {
            time--;
            endpoint.TakeDamage(damagePerSecond);

            yield return new WaitForSeconds(1f);
        }

        yield return 0;
    }
}
