﻿using UnityEngine;

[System.Serializable]
public class EnemyGroup
{
    public GameObject enemy;
    public int count;
}
