﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// THIS ENEMYMOVEMENT SCRIPT LOOPS:
// SPECIFICALLY FOR ALCOHOL LEVEL

[RequireComponent(typeof(Enemy))]
public class EnemyMovement : MonoBehaviour
{
    private Transform target;
    private int waypointIndex = 0;

    private Enemy enemy;
    [HideInInspector]
    public bool isGoingBack;
    [HideInInspector]
    public bool isAttacking;
    [HideInInspector]
    public bool isGoingNextStage;
    private int attackTime;

    void Start()
    {
        enemy = GetComponent<Enemy>();
        target = Waypoints.points[0];
        isGoingBack = false;
        isAttacking = false;
        isGoingNextStage = false;
        attackTime = 5;
    }

    void Update()
    {
        if (NewJournalUI.instance.GetComponent<Canvas>().enabled)
        {
            return;
        }

        if (isAttacking)
            return;

        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);
        transform.rotation = Quaternion.LookRotation(dir);

        // ensures healthCanvas don't rotate with the enemy
        enemy.healthCanvas.rotation = Quaternion.Euler(40, 0, 0);
        enemy.healthCanvas.position = transform.position + new Vector3(0f, 4f, 0.71f);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            if (isGoingNextStage)
                GetNextWaypointNextStage();

            if (!isGoingBack)
                GetNextWaypoint();
            else
                GetNextWaypointBack();
        }

        enemy.speed = enemy.startSpeed;
    }

    void GetNextWaypoint()
    {
        if (waypointIndex >= Waypoints.points.Length - 1)
        {
            EndPath();
            return;
        }

        waypointIndex++;
        target = Waypoints.points[waypointIndex];
    }

    void GetNextWaypointBack()
    {
        if (waypointIndex >= Waypoints.pointsBack.Length - 1)
        {
            EndPath();
            return;
        }

        waypointIndex++;
        target = Waypoints.pointsBack[waypointIndex];
    }

    void GetNextWaypointNextStage()
    {
        if (waypointIndex >= Waypoints.pointsNextStage.Length - 1)
        {
            Destroy(gameObject);
            return;
        }

        waypointIndex++;
        target = Waypoints.pointsNextStage[waypointIndex];
    }

    void EndPath()
    {
        waypointIndex = 0;
        if (!isGoingBack)
        {
            isAttacking = true;
            StartCoroutine(enemy.Damage(Endpoint.instance, attackTime));
            Invoke("StopAttacking", attackTime);
            target = Waypoints.pointsBack[waypointIndex];
            isGoingBack = true;
        } else
        {
            target = Waypoints.points[waypointIndex];
            isGoingBack = false;
        }
    }

    public void GoToNextStage()
    {
        waypointIndex = 0;
        target = Waypoints.pointsNextStage[waypointIndex];
        isGoingNextStage = true;
    }

    void StopAttacking()
    {
        isAttacking = false;
    }
}
