﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

    private Transform target;
    private Enemy targetEnemy;

    [Header("General")]
    public float range = 15f;

    [Header("Use Bullets (default)")]
    public GameObject bulletPrefab;
    public float fireRate = 1f;
    private float fireCountdown = 0f;

    [Header("Use Laser")]
    public bool useLaser = false;

    public float damageOverTime = 30;
    public float slowAmount = .5f;

    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;

    [Header("Unity Setup Fields")]
    public string enemyTag = "Enemy";

    public Transform hDrive; // horizontal drive
    public Transform gun;   // gun
    public float turnSpeed = 10f;

    public Transform firePoint;

    [HideInInspector]
    public Ability damageBuff, DoTBuff, fireRateBuff, rangeBuff, slowDebuff;
    [HideInInspector]
    public Ability activeFreeze;

    public float startDoT, startRange, startFireRate;

    private Animator anim;
    private AudioSource audioSource;
    private bool isLaserAudioPlayed = false;

    void Awake()
    {
        startDoT = damageOverTime;
        startRange = range;
        startFireRate = fireRate;
        anim = gameObject.GetComponent<Animator>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    void Start () {
        
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            EnemyMovement em = enemy.GetComponent<EnemyMovement>();
            if (distanceToEnemy < shortestDistance
                && ((!em.isGoingNextStage
                && !em.isGoingBack) || em.isAttacking))
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        

        if(nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
            if(useLaser && !isLaserAudioPlayed)
            {
                audioSource.Play();
                isLaserAudioPlayed = true;
            }
        } else
        {
            target = null;
        }
    }
	
	void Update () {
        if (NewJournalUI.instance.GetComponent<Canvas>().enabled)
        {
            return;
        }

        UpdateTarget();

        if (damageBuff.quantity <= 0 || damageBuff.pct <= 0)
            damageBuff.isActive = false;
        if (DoTBuff.quantity <= 0 || DoTBuff.pct <= 0)
            DoTBuff.isActive = false;
        if (slowDebuff.quantity <= 0 || slowDebuff.pct <= 0)
            slowDebuff.isActive = false;
        if (fireRateBuff.quantity <= 0 || fireRateBuff.pct <= 0)
            fireRateBuff.isActive = false;
        if (rangeBuff.quantity <= 0 || rangeBuff.pct <= 0)
            rangeBuff.isActive = false;

        if (target == null)
        {
            if (useLaser)
            {
                if (lineRenderer.enabled)
                {
                    isLaserAudioPlayed = false;
                    lineRenderer.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled = false;
                }
            }

            return;
        }

        LockOnTarget();

        if (useLaser)
        {
            Laser();
        }
        else
        {
            if (fireCountdown <= 0f)
            {
                Shoot();
                fireCountdown = 1f / fireRate;
            }

            fireCountdown -= Time.deltaTime;
        }
    }

    void LockOnTarget()
    {
        Vector3 targetPositionCopy = new Vector3(target.position.x, target.position.y + 0.75f, target.position.z);  // because enemy's pivot is under the enemy
        Vector3 dir = targetPositionCopy - firePoint.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);

        Vector3 horizontalRotation = Quaternion.Lerp(hDrive.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        hDrive.rotation = Quaternion.Euler(0f, horizontalRotation.y, 0f);

        Vector3 verticalRotation = Quaternion.Lerp(gun.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        gun.rotation = Quaternion.Euler(verticalRotation.x, horizontalRotation.y, 0f);
    }

    void Laser()
    {
        if (anim != null)
            anim.Play("Fire_Loop");

        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);
        targetEnemy.Slow(slowAmount);

        if (slowDebuff.isActive)
            targetEnemy.MoreSlow(slowDebuff.pct);

        if (activeFreeze.isActive)
            targetEnemy.Freeze();

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled = true;
        }
        Vector3 targetPositionCopy = new Vector3(target.position.x, target.position.y + 0.75f, target.position.z);

        lineRenderer.SetPosition(0, firePoint.position);
        lineRenderer.SetPosition(1, targetPositionCopy);

        Vector3 dir = firePoint.position - targetPositionCopy;

        impactEffect.transform.position = targetPositionCopy + dir.normalized * .5f;

        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
        
    }

    void Shoot()
    {
        if(audioSource != null)
            audioSource.Play();

        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if (damageBuff.isActive)
            BuffDamageBullet(bullet, damageBuff.pct);

        if (DoTBuff.isActive)
        {
            bullet.isDoT = true;
            bullet.DoTPct = DoTBuff.pct;
            bullet.DoTDuration = DoTBuff.duration;
        }

        if (slowDebuff.isActive)
        {
            bullet.isSlowing = true;
            bullet.slowPct = slowDebuff.pct;
            bullet.slowDuration = slowDebuff.duration;
        }

        if (activeFreeze.isActive)
        {
            bullet.isFreezing = true;
            bullet.freezeDuration = activeFreeze.duration;
        }

        if (bullet != null)
        {
            if (anim != null)
                anim.Play("Fire");
            bullet.Seek(target);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void BuffDamageBullet(Bullet bullet, float damageBuffPct)
    {
        bullet.damage += bullet.damage * damageBuffPct / 100;
    }

    public void BuffDamageLaser(float damageBuffPct)
    {
        damageOverTime = startDoT + startDoT * damageBuffPct / 100;
    }

    public void BuffDoTLaser(float DoTBuffPct)
    {
        damageOverTime = startDoT + startDoT * DoTBuffPct / 100;
    }

    public void BuffRange(float rangeBuffPct)
    {
        range = startRange + startRange * rangeBuffPct / 100;
    }

    public void BuffFireRate(float fireRateBuffPct)
    {
        fireRate = startFireRate + startFireRate * fireRateBuffPct / 100;
    }
}
