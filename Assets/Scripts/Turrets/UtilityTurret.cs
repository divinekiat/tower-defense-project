﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UtilityTurret : MonoBehaviour {

    [Header("General")]
    public float range = 1f;

    [Header("Unity Setup Fields")]
    public string nodeTag = "Node";

    [Header("Passive Abilities")]
    public bool damageBuff = false;
    public float damageBuffPct = 0; // percentage, relative to root prefab

    public bool rangeBuff = false;
    public float rangeBuffPct = 0;

    public bool fireRateBuff = false;
    public float fireRateBuffPct = 0;

    public bool DoTBuff = false;
    public float DoTBuffPct = 0;
    public float DoTBuffDuration = 0; // for bullets

    public bool slowDebuff = false;
    public float slowDebuffPct = 0;
    public float slowDebuffDuration = 0;    // for bullets

    // Active Abilities
    [Header("Active Abilities")]
    public float activeCooldown = 5f;
    [HideInInspector]
    public float cooldownFillAmount = 1f;

    public bool activeDamageBuff = false;
    public float activeDamageBuffPct = 0; // percentage, relative to root prefab
    public float activeDamageBuffDuration = 0;

    public bool activeRangeBuff = false;
    public float activeRangeBuffPct = 0;
    public float activeRangeBuffDuration = 0;

    public bool activeFreeze = false;
    public float activeFreezeBulletDuration = 0;    // how long is the freeze effect given by the bullet
    public float activeFreezeDuration = 0; // how long the turret shoots freezing stuff, applies to the laser turret

    private Vector3 nodePosition;
    private Vector3 rangeVector;

    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;

    private Animator anim;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();

        range *= 4;
        range += 0.5f;
        rangeVector = new Vector3(range * 2, range * 2, range * 2);
        nodePosition = transform.position;

        minX = nodePosition.x - range;
        maxX = nodePosition.x + range;
        minZ = nodePosition.z - range;
        maxZ = nodePosition.z + range;

        AffectArea();
    }

    void AffectArea()
    {
        GameObject[] nodes = GameObject.FindGameObjectsWithTag(nodeTag);
        foreach (GameObject nodeGO in nodes)
        {
            Vector3 thisPosition = nodeGO.transform.position;

            if (thisPosition.x >= minX && thisPosition.x <= maxX &&
                thisPosition.z >= minZ && thisPosition.z <= maxZ)
            {
                Node node = nodeGO.GetComponent<Node>();
                node.isInUtilityRange = true;
                node.affectingUtilityTurrets.Add(this);
                AffectNode(node);

            }

        }
    }

    public void ActiveAffectArea()
    {
        if (anim != null)
            anim.Play("Active_Start");

        if (audioSource != null)
            audioSource.Play();

        StartCoroutine(Cooldown());
        GameObject[] nodes = GameObject.FindGameObjectsWithTag(nodeTag);
        foreach (GameObject nodeGO in nodes)
        {
            Vector3 thisPosition = nodeGO.transform.position;

            if (thisPosition.x >= minX && thisPosition.x <= maxX &&
                thisPosition.z >= minZ && thisPosition.z <= maxZ)
            {
                Node node = nodeGO.GetComponent<Node>();
                ActiveAffectNode(node);

            }

        }
    }

    public void RevertArea()
    {
        Debug.Log("Reverting Area");

        StartCoroutine(Cooldown());
        GameObject[] nodes = GameObject.FindGameObjectsWithTag(nodeTag);
        foreach (GameObject nodeGO in nodes)
        {
            Vector3 thisPosition = nodeGO.transform.position;

            if (thisPosition.x >= minX && thisPosition.x <= maxX &&
                thisPosition.z >= minZ && thisPosition.z <= maxZ)
            {
                Node node = nodeGO.GetComponent<Node>();
                RevertNode(node);
                node.affectingUtilityTurrets.Remove(this);
            }

        }
    }

    public void RevertNode(Node node)
    {
        if (node.turret == null)
            return;

        Turret turret = node.turret.GetComponent<Turret>();

        if (turret == null)
            return;

        if (damageBuff)
        {
            turret.damageBuff.quantity--;
            turret.damageBuff.pct -= damageBuffPct;
            turret.BuffDamageLaser(turret.damageBuff.pct);
        }

        if (rangeBuff)
        {
            turret.rangeBuff.quantity--;
            turret.rangeBuff.pct -= rangeBuffPct;
            turret.BuffRange(turret.rangeBuff.pct);
        }

        if (fireRateBuff)
        {
            turret.fireRateBuff.quantity--;
            turret.fireRateBuff.pct -= fireRateBuffPct;
            turret.BuffFireRate(turret.fireRateBuff.pct);
            turret.BuffDoTLaser(turret.fireRateBuff.pct);
        }

        if (slowDebuff)
        {
            turret.slowDebuff.quantity--;
            turret.slowDebuff.pct -= slowDebuffPct;
            turret.slowDebuff.duration -= slowDebuffDuration;
        }

        if (DoTBuff)
        {
            turret.DoTBuff.quantity--;
            turret.DoTBuff.pct -= DoTBuffPct;
            turret.DoTBuff.duration -= DoTBuffDuration;
            turret.BuffDoTLaser(turret.DoTBuff.pct);
        }
    }

    public void AffectNode(Node node)
    {

        if (node.turret == null)
            return;

        Turret turret = node.turret.GetComponent<Turret>();

        if (turret == null)
            return;

        if (damageBuff)
        {
            BuffDamage(turret);
        }

        if (rangeBuff)
        {
            BuffRange(turret);
        }

        if (fireRateBuff)
        {
            BuffFireRate(turret);
        }

        if (slowDebuff)
        {
            SlowEnemy(turret);
        }

        if (DoTBuff)
        {
            BuffDoT(turret);
        }
    }

    void ActiveAffectNode(Node node)
    {

        if (node.turret == null)
            return;

        Turret turret = node.turret.GetComponent<Turret>();

        if (turret == null)
            return;

        if (activeDamageBuff)
        {
            StartCoroutine(ActiveBuffDamage(turret));
        }

        if (activeRangeBuff)
        {
            StartCoroutine(ActiveBuffRange(turret));
        }

        if (activeFreeze)
        {
            StartCoroutine(ActiveFreeze(turret));
        }
    }

    void BuffDamage(Turret turret)
    {
        turret.damageBuff.isActive = true;
        turret.damageBuff.quantity++;
        turret.damageBuff.pct += damageBuffPct;
        turret.BuffDamageLaser(turret.damageBuff.pct);
    }

    void BuffRange(Turret turret)
    {
        turret.rangeBuff.isActive = true;
        turret.rangeBuff.quantity++;
        turret.rangeBuff.pct += rangeBuffPct;
        turret.BuffRange(turret.rangeBuff.pct);
    }

    void BuffFireRate(Turret turret)
    {
        turret.fireRateBuff.isActive = true;
        turret.fireRateBuff.quantity++;
        turret.fireRateBuff.pct += fireRateBuffPct;
        turret.BuffFireRate(turret.fireRateBuff.pct);
        turret.BuffDoTLaser(turret.fireRateBuff.pct);
    }

    void SlowEnemy(Turret turret)
    {
        turret.slowDebuff.isActive = true;
        turret.slowDebuff.quantity++;
        turret.slowDebuff.pct += slowDebuffPct;
        turret.slowDebuff.duration += slowDebuffDuration;
    }

    void BuffDoT(Turret turret)
    {
        turret.DoTBuff.isActive = true;
        turret.DoTBuff.quantity++;
        turret.DoTBuff.pct += DoTBuffPct;
        turret.DoTBuff.duration += DoTBuffDuration;
        turret.BuffDoTLaser(turret.DoTBuff.pct);
    }

    IEnumerator ActiveBuffDamage(Turret turret)
    {
        turret.damageBuff.isActive = true;
        turret.damageBuff.quantity++;
        turret.damageBuff.pct += activeDamageBuffPct;
        turret.BuffDamageLaser(turret.damageBuff.pct);
        yield return new WaitForSeconds(activeDamageBuffDuration);
        turret.damageBuff.quantity--;
        turret.damageBuff.pct -= activeDamageBuffPct;
        turret.BuffDamageLaser(turret.damageBuff.pct);
        if (anim != null)
            anim.Play("Active_End");
    }

    IEnumerator ActiveBuffRange(Turret turret)
    {
        turret.rangeBuff.isActive = true;
        turret.rangeBuff.quantity++;
        turret.rangeBuff.pct += activeRangeBuffPct;
        turret.BuffRange(turret.rangeBuff.pct);
        yield return new WaitForSeconds(activeRangeBuffDuration);
        turret.rangeBuff.quantity--;
        turret.rangeBuff.pct -= activeRangeBuffPct;
        turret.BuffRange(turret.rangeBuff.pct);
        if (anim != null)
            anim.Play("Active_End");
    }

    IEnumerator ActiveFreeze(Turret turret)
    {
        turret.activeFreeze.isActive = true;
        turret.activeFreeze.quantity++;
        turret.activeFreeze.duration += activeFreezeBulletDuration;
        yield return new WaitForSeconds(activeFreezeDuration);
        turret.activeFreeze.quantity--;
        if(turret.activeFreeze.quantity <= 0)
            turret.activeFreeze.isActive = false;
        turret.activeFreeze.duration--;
        if (anim != null)
            anim.Play("Active_End");
    }

    IEnumerator Cooldown()
    {
        float cooldown = activeCooldown;
        while (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            cooldownFillAmount = 1 - cooldown / activeCooldown;
            yield return null;
        }

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(nodePosition, rangeVector);
    }
}
