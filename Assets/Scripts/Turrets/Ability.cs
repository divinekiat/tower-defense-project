﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ability {

    public bool isActive = false;
    public float pct = 0;
    public float duration = 0;
    public int quantity = 0;

}