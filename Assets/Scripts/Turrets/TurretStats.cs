﻿using System.Collections;
using UnityEngine;

// CHANGE the DeepCopy method in Node.cs if adding new field
[System.Serializable]
public class TurretStats {

    public GameObject prefab;
    public string name = "";
    public int cost = 0;    // cost to buy
    public bool isLocked = true;
    [TextArea]
    public string description;
    [HideInInspector]
    public bool isPurchased = false;
}
