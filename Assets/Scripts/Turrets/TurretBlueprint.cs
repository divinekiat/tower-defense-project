﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class TurretBlueprint
{
    public string type = "ST";

    public TurretStats root;

    public TechTree techTree;

    // tech tree lines 
    public TurretStats[] A = new TurretStats[3];
    public TurretStats[] B = new TurretStats[3];
    public TurretStats[] C = new TurretStats[3];
}
