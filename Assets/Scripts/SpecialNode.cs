﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialNode : MonoBehaviour {

    public bool damageBuff = false;
    public float damageBuffPct = 0; // percentage, relative to root prefab

    public bool rangeBuff = false;
    public float rangeBuffPct = 0;

    public bool fireRateBuff = false;
    public float fireRateBuffPct = 0;

    public bool DoTBuff = false;
    public float DoTBuffPct = 0;
    public float DoTBuffDuration = 0; // for bullets

    public bool slowDebuff = false;
    public float slowDebuffPct = 0;
    public float slowDebuffDuration = 0;    // for bullets

    public void AffectNode(Node node)
    {

        if (node.turret == null)
            return;

        Turret turret = node.turret.GetComponent<Turret>();

        if (turret == null)
            return;

        if (damageBuff)
        {
            BuffDamage(turret);
        }

        if (rangeBuff)
        {
            BuffRange(turret);
        }

        if (fireRateBuff)
        {
            BuffFireRate(turret);
        }

        if (slowDebuff)
        {
            SlowEnemy(turret);
        }

        if (DoTBuff)
        {
            BuffDoT(turret);
        }
    }

    void BuffDamage(Turret turret)
    {
        turret.damageBuff.isActive = true;
        turret.damageBuff.quantity++;
        turret.damageBuff.pct += damageBuffPct;
        turret.BuffDamageLaser(turret.damageBuff.pct);
    }

    void BuffRange(Turret turret)
    {
        turret.rangeBuff.isActive = true;
        turret.rangeBuff.quantity++;
        turret.rangeBuff.pct += rangeBuffPct;
        turret.BuffRange(turret.rangeBuff.pct);
    }

    void BuffFireRate(Turret turret)
    {
        turret.fireRateBuff.isActive = true;
        turret.fireRateBuff.quantity++;
        turret.fireRateBuff.pct += fireRateBuffPct;
        turret.BuffFireRate(turret.fireRateBuff.pct);
        turret.BuffDoTLaser(turret.fireRateBuff.pct);
    }

    void SlowEnemy(Turret turret)
    {
        turret.slowDebuff.isActive = true;
        turret.slowDebuff.quantity++;
        turret.slowDebuff.pct += slowDebuffPct;
        turret.slowDebuff.duration += slowDebuffDuration;
    }

    void BuffDoT(Turret turret)
    {
        turret.DoTBuff.isActive = true;
        turret.DoTBuff.quantity++;
        turret.DoTBuff.pct += DoTBuffPct;
        turret.DoTBuff.duration += DoTBuffDuration;
        turret.BuffDoTLaser(turret.DoTBuff.pct);
    }
}
