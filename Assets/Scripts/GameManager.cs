﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static bool GameIsOver;

    private GameObject gameOverUI;
    private GameObject gameOverUIChild;
    private GameObject completeLevelUI;
    private GameObject completeLevelUIChild;
    private GameObject completeModeUI;
    private GameObject completeModeUIChild;

    public string nextLevel = "Alcohol02";
    public int levelToUnlock = 2;

    private SceneFader sceneFader;

    public bool isLastLevel = false;
    public int totalQuizHPS = 30;

    void Start()
    {
        sceneFader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
        gameOverUI = GameObject.Find("GameOver");
        gameOverUIChild = gameOverUI.transform.GetChild(0).gameObject;
        completeLevelUI = GameObject.Find("CompleteLevel");
        completeLevelUIChild = completeLevelUI.transform.GetChild(0).gameObject;
        completeModeUI = GameObject.Find("CompleteMode");
        completeModeUIChild = completeModeUI.transform.GetChild(0).gameObject;
        GameIsOver = false;
    }

    void Update()
    {
        if (GameIsOver)
            return;

        if (Endpoint.Health <= 0)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        GameIsOver = true;
        gameOverUIChild.SetActive(true);
    }

    public void WinLevel()
    {
        GameIsOver = true;

        if (gameOverUIChild.activeSelf)  // prevents winning if player already lost
            return;

        completeLevelUIChild.SetActive(true);
    }

    public void WinMode()
    {
        GameIsOver = true;

        if (gameOverUIChild.activeSelf)  // prevents winning if player already lost
            return;

        completeModeUIChild.SetActive(true);
    }
}
