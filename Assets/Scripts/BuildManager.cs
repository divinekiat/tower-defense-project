﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;

    void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
    }

    public GameObject buildEffect;
    public GameObject sellEffect;
    public GameObject shop;

    public Color selectedNodeColor;

    private TurretBlueprint turretToBuild;
    [HideInInspector]
    public Node selectedNode;
    private TechTree techTree;
    private GameObject techTreeUI;

    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasResources { get { return PlayerStats.Resources >= turretToBuild.root.cost; } }

    private Text primaryTextDisplay;
    private Text secondaryTextDisplay;

    void Start()
    {
        primaryTextDisplay = GameObject.Find("PrimaryTextDisplay").GetComponent<Text>();
        secondaryTextDisplay = GameObject.Find("SecondaryTextDisplay").GetComponent<Text>();
    }

    public void SelectNode(Node node)
    {
        if(selectedNode == node)
        {
            DeselectNode();
            return;
        }

        if (techTree != null)
        {
            techTree.Hide();        // disables current if there exists
            if (selectedNode != null)
            {
                for (int i = 0; i < selectedNode.transform.childCount; i++)
                {
                    selectedNode.rends[i].material.color = selectedNode.startColors[i];
                }
            }            
        }

        selectedNode = node;

        techTree = node.techTree;

        techTreeUI = techTree.ui;
        techTreeUI.SetActive(true);
        
        shop.SetActive(false);

        for (int i = 0; i < selectedNode.transform.childCount; i++)
        {
            selectedNode.rends[i].material.color = selectedNodeColor;
        }

        turretToBuild = null;

        techTree.SetTarget(node);        
    }

    public void DeselectNode()
    {
        for (int i = 0; i < selectedNode.transform.childCount; i++)
        {
            selectedNode.rends[i].material.color = selectedNode.startColors[i];
        }

        selectedNode = null;

        if(techTree != null)
        {
            techTree.Hide();
            techTree.closeButton.SetActive(false);
        }

        shop.SetActive(true);
    }

    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
        FlashTextDisplay(turretToBuild.root.name, 0);
        FlashTextDisplay(turretToBuild.root.description, 1);
        if (selectedNode == null) return;
        DeselectNode();
    }

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }

    public void SetTurretToBuildToNull()
    {
        turretToBuild = null;
    }

    // i = 0, change primary text
    // i = 1, change secondary text
    public void FlashTextDisplay(string s, int i)
    {
        StopAllCoroutines();

        if (i == 0)
        {
            primaryTextDisplay.GetComponent<Text>().text = s;
            primaryTextDisplay.enabled = true;
        }
        else if (i == 1)
        {
            secondaryTextDisplay.GetComponent<Text>().text = s;
            secondaryTextDisplay.enabled = true;
        }

        StartCoroutine(CloseTextDisplay());
    }

    IEnumerator CloseTextDisplay()
    {
        yield return new WaitForSecondsRealtime(3);
        primaryTextDisplay.enabled = false;
        secondaryTextDisplay.enabled = false;
    }
}
