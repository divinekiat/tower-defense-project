﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullet : MonoBehaviour {

    private Transform target;

    public float speed = 70f;

    public float damage = 50;

    public GameObject impactEffect;

    [Header("Missile fields")]
    public float explosionRadius = 0f;
    public float launchAngle = 0f;
    private Rigidbody rigid;

    [HideInInspector]
    public bool isSlowing = false;
    [HideInInspector]
    public float slowPct = 0f;
    [HideInInspector]
    public float slowDuration = 0f;

    [HideInInspector]
    public bool isFreezing = false;
    [HideInInspector]
    public float freezeDuration = 0f;

    [HideInInspector]
    public bool isDoT = false;
    [HideInInspector]
    public float DoTPct = 0f;
    [HideInInspector]
    public float DoTDuration = 0f;

    private bool targetHit = false;

    public void Seek(Transform _target)
    {
        target = _target;
        targetHit = false;
    }

	// Update is called once per frame
	void Update () {

		if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 targetPositionCopy = new Vector3(target.position.x, target.position.y + 0.75f, target.position.z); // because enemy's pivot is under the enemy
    
        Vector3 dir = targetPositionCopy - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            if (targetHit)
                return;
            HitTarget();
            targetHit = true;
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);
    }


    void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effectIns, 2f);

        GetComponent<MeshRenderer>().enabled = false; // makes bullet invisible

        if (explosionRadius > 0f)
        {
            Explode();
        } else
        {
            Damage(target);
        }

        if(!isSlowing && !isFreezing && !isDoT)
            Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();

        if(e != null)
        {
            if (!isDoT)
            {
                e.TakeDamage(damage);
            }  
            else
            {
                float DoTDamage = damage + damage * DoTPct / 100;
                StartCoroutine(ApplyDoT(e, DoTDamage, DoTDuration));
            }

            if (isFreezing)
            {
                StartCoroutine(ApplyFreeze(e, freezeDuration));
            }

            if (isSlowing)
            {
                Debug.Log("Slow duration: " + slowDuration);
                StartCoroutine(ApplySlow(e, slowPct, slowDuration));
            }
           
        }
        
    }

    IEnumerator ApplyDoT(Enemy enemy, float DoTDamage, float duration)
    {
        while (duration > 0f)
        {
            duration -= Time.deltaTime;
            enemy.TakeDamage(DoTDamage);
            yield return new WaitForSeconds(1);
        }

        Destroy(gameObject);
    }

    IEnumerator ApplySlow(Enemy enemy, float pct, float duration)
    {
        while (duration > 0f)
        {
            duration -= Time.deltaTime;
            enemy.Slow(pct);
            yield return null;
        }

        Destroy(gameObject);
    }

    IEnumerator ApplyFreeze(Enemy enemy, float duration)
    {
        while (duration > 0f)
        {
            duration -= Time.deltaTime;
            enemy.Freeze();
            yield return null;
        }

        Destroy(gameObject);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
    
}
