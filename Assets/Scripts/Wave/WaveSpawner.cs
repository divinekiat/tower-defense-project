﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour
{

    public static int EnemiesAlive = 0;

    public bool isTimeMode = false;

    public Wave[] waves;

    private Transform spawnPoint;

    public float timeBetweenWaves = 5f;
    private float countdown = 10f;
    private float quizCountdown = 5f;

    private Text waveCountdownText;
    private Text date;  // WILL BE JOURNAL NUMBER

    [HideInInspector]
    public int waveIndex = 0;

    private GameManager gameManager;

    private Wave wave;

    [HideInInspector]
    public DoctorDialogueTrigger doctorTrigger;

    private GameObject hugeTextDisplay;
    private Text hugeTextDisplayText;

    [HideInInspector]
    public Image screenLocker;

    public bool showIntro = false;

    public static AudioSource audioSource;

    public AudioClip backgroundMusic;
    private bool backgroundMusicIsStarted = false;

    [HideInInspector]
    public int lastJournalPage = 0;

    private GameObject pauseMenuUI;

    private Shop shop;

    private int quizNoAnswered;
    private GameObject startStageButton;

    void Start()
    {
        startStageButton = GameObject.Find("StartStageParent").transform.GetChild(0).gameObject;
        quizNoAnswered = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered");
        pauseMenuUI = gameObject.GetComponent<PauseMenu>().ui;
        waveCountdownText = GameObject.Find("Countdown").GetComponent<Text>();
        date = GameObject.Find("Date").GetComponent<Text>();
        hugeTextDisplay = GameObject.Find("HugeText");
        hugeTextDisplayText = hugeTextDisplay.GetComponent<Text>();
        screenLocker = GameObject.Find("ScreenLocker").GetComponent<Image>();
        spawnPoint = GameObject.Find("SpawnPoint").transform;
        gameManager = gameObject.GetComponent<GameManager>();
        audioSource = gameObject.GetComponent<AudioSource>();
        waveCountdownText.text = string.Format("{0:00.00}", 10.0f);
        EnemiesAlive = 0;
        date.text = "Journal #" + (waves[0].journalNo);
        doctorTrigger = FindObjectOfType<DoctorDialogueTrigger>();
        lastJournalPage = waves[0].journalNo - 1;
        Debug.Log(quizNoAnswered);
        if (quizNoAnswered > 0)
        {
            startStageButton.SetActive(true);
            return;
        }            

        if (showIntro)
            doctorTrigger.TriggerIntroDialogue();
        else
        { 
            NewJournalUI.instance.OpenJournalUI();
            NewJournalUI.instance.SetPage(lastJournalPage);
        }

    }

    void Update()
    {

        if (NewJournalUI.instance.GetComponent<Canvas>().enabled || 
            QuizManager.instance.GetComponent<Canvas>().enabled ||
            FindObjectOfType<DialogueManager>().animator.GetBool("IsOpen") ||
            pauseMenuUI.activeSelf ||
            startStageButton.activeSelf)
        {
            return;
        }        

        if (!isTimeMode)
        {
            if (EnemiesAlive > 0)
            {
                return;
            }
        }

        quizNoAnswered = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered");

        if (wave != null)
        {
            if (EnemiesAlive <= 0
                && !QuizManager.instance.GetComponent<Canvas>().enabled
                && !QuizManager.instance.questionIsAnswered
                && !hugeTextDisplayText.isActiveAndEnabled)
            {
                FlashHugeTextDisplay("Wave Finished!");
                // flashing of the quiz happens after flashing the huge text
                // see below: FlashHugeTextDisplay()
                return;
            }
        }

        if (wave != null 
            && (!(waveIndex < quizNoAnswered))
            && ((QuizManager.instance.questionIsAnswered)
                || (!wave.showQuiz)))
        {
            if(wave.journalNo != 0)
            {
                lastJournalPage = wave.journalNo;
                NewJournalUI.instance.OpenJournalUI();
                NewJournalUI.instance.SetPage(lastJournalPage);
                date.text = "Journal #" + (wave.journalNo + 1);
                wave.journalNo = 0;
            }
        }

        if (!doctorTrigger.isShown 
            && EnemiesAlive <= 0 
            && !NewJournalUI.instance.GetComponent<Canvas>().enabled
            && !hugeTextDisplayText.isActiveAndEnabled)
        {
            
            doctorTrigger.TriggerDialogue();
            backgroundMusicIsStarted = false;
        }


        if (countdown <= 0f)
        {
            if (waveIndex == waves.Length)
                return;

            PlayerStats.Rounds++;

            wave = waves[waveIndex];
            date.text = "Journal #" + wave.journalNo;
            wave.count = 0;

            for (int i = 0; i < wave.enemyGroup.Length; i++)
            {
                for (int j = 0; j < wave.enemyGroup[i].count; j++)
                {
                    wave.count++;
                }
            }            

            if (isTimeMode)
                EnemiesAlive += wave.count;
            else
                EnemiesAlive = wave.count;

            StartCoroutine(SpawnWave(wave));

            if (isTimeMode)
                countdown = wave.timeBeforeNextWave;
            else
            {
                countdown = timeBetweenWaves;

            }

            QuizManager.instance.questionIsAnswered = false; // to change it back after popping out the journal
            NewJournalUI.instance.afterClosed = false;
            doctorTrigger.isShown = false;
            return;
        }

        // make sure countdown doesn't start when things are there
        if (hugeTextDisplayText.isActiveAndEnabled 
            || NewJournalUI.instance.GetComponent<Canvas>().enabled
            || QuizManager.instance.GetComponent<Canvas>().isActiveAndEnabled
            || FindObjectOfType<DialogueManager>().animator.GetBool("IsOpen"))
            return;

        if(!backgroundMusicIsStarted)
        {
            audioSource.clip = backgroundMusic;
            audioSource.Play();
            backgroundMusicIsStarted = true;
        } 

        countdown -= Time.unscaledDeltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);        

        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave(Wave wave)
    {
        for (int i = 0; i < wave.enemyGroup.Length; i++)
        {
            for (int j = 0; j < wave.enemyGroup[i].count; j++)
            {
                SpawnEnemy(wave.enemyGroup[i].enemy);
                yield return new WaitForSeconds(1f / wave.rate);
            }
        }

        waveIndex++;
    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
    }

    public void FlashHugeTextDisplay(string s)
    {
        hugeTextDisplayText.text = s;
        hugeTextDisplayText.enabled = true;
        screenLocker.enabled = true;
        BuildManager.instance.SetTurretToBuildToNull();
        quizCountdown = 5.0f;
        waveCountdownText.text = string.Format("{0:00.00}", quizCountdown);
        StartCoroutine(CloseTextDisplay());
    }

    IEnumerator CloseTextDisplay()
    {
        quizNoAnswered = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "quizNoAnswered");

        if (!(waveIndex <= quizNoAnswered))
        {
            while (quizCountdown > 0)
            {
            
                date.text = "Quiz starting in... ";
                quizCountdown -= Time.unscaledDeltaTime;
                waveCountdownText.text = string.Format("{0:00.00}", quizCountdown);
                yield return null;
            }
        } else
        {
            yield return new WaitForSecondsRealtime(5);
        }

        waveCountdownText.text = string.Format("{0:00.00}", 5.0f);
        date.text = "Journal #" + (wave.journalNo);

        screenLocker.enabled = false;
        hugeTextDisplayText.enabled = false;
        if(wave.showQuiz)
        {
            if(!(waveIndex <= quizNoAnswered))
            {
                QuizManager.instance.ShowQuestion();
            } else
            {
                QuizManager.instance.questionIsAnswered = true;
                NewJournalUI.instance.afterClosed = true;

                if (waveIndex == waves.Length
                    && EnemiesAlive <= 0)
                {
                    if (gameManager.isLastLevel)
                    {
                        gameManager.WinMode();
                    }
                    else
                    {
                        gameManager.WinLevel();
                    }
                    this.enabled = false;
                }
            }
            
        } else
        {
            doctorTrigger.currentFactIndex++;
        }           
    }
}
