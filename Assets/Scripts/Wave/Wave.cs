﻿using UnityEngine;

[System.Serializable]
public class Wave
{
    public EnemyGroup[] enemyGroup;
    public int count;
    public float rate;
    public string date;
    public int timeBeforeNextWave;
    public int journalNo = 0; // 0 means no journal entry
    public bool showQuiz = false;
}
