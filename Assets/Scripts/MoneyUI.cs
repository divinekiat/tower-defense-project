﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyUI : MonoBehaviour {

    public Text resourcesText;
    public Text moneyText;
	
	// Update is called once per frame
	void Update () {
        resourcesText.text = PlayerStats.Resources.ToString();
        moneyText.text = PlayerStats.Money.ToString();
	}
}
