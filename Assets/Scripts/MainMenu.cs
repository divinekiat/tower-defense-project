﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{ 
    public string levelToLoad = "MainLevel";
    public string creditsScene = "Credits";
    public string tutorialScene = "Tutorial";

    private SceneFader sceneFader;

    private void Awake()
    {
        PlayerPrefs.SetString("previousScene", "mainMenu");
        sceneFader = GameObject.Find("SceneFader").GetComponent<SceneFader>();
    }

    public void Play()
    {
        sceneFader.FadeTo(levelToLoad);
    }

    public void Credits()
    {
        sceneFader.FadeTo(creditsScene);
    }

    public void Tutorial()
    {
        sceneFader.FadeTo(tutorialScene);
    }

    public void Quit()
    {
        Debug.Log("Exiting...");
        Application.Quit();
    }
}