﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Node : MonoBehaviour {

    public Vector3 positionOffset;
    public Color notEnoughResourcesColor;
    public AudioClip buildSound;
    public AudioClip sellSound;
    public AudioClip upgradeSound;

    [HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint turretBlueprint;
    [HideInInspector]
    public TechTree techTree;
    [HideInInspector]
    public GameObject techTreeUI;
    [HideInInspector]
    public TurretStats currentTS;
    [HideInInspector]
    public int worth;   // add every time turret is upgraded
    [HideInInspector]
    public TurretStats[] A, B, C;
    [HideInInspector]
    public bool isInUtilityRange = false;

    [HideInInspector]
    public List<UtilityTurret> affectingUtilityTurrets;

    [HideInInspector]
    public int AIndex, BIndex, CIndex = 0;
    [HideInInspector]
    public bool isAMaxed, isBMaxed, isCMaxed = false;

    [HideInInspector]
    public bool isActiveAbilityChosen = false; // for active utility turret
    [HideInInspector]
    public string ActiveAbilityLineChosen = ""; // what active ability chosen
    [HideInInspector]
    public bool isUpgraded = false; // if the other turrets are upgraded
    [HideInInspector]
    public string turretLineChosen = "";

    [HideInInspector]
    public Renderer[] rends;
    [HideInInspector]
    public Color[] startColors;
    private Color grey;

    BuildManager buildManager;

    private AudioSource audioSource;
    private Image screenLocker;

    void Start()
    {
        screenLocker = GameObject.Find("ScreenLocker").GetComponent<Image>();
        audioSource = gameObject.AddComponent<AudioSource>();
        buildSound = Resources.Load<AudioClip>("Build");
        upgradeSound = Resources.Load<AudioClip>("Build");
        sellSound = Resources.Load<AudioClip>("Build");

        AIndex = 0;
        BIndex = 0;
        CIndex = 0;

        isAMaxed = false;
        isBMaxed = false;
        isCMaxed = false;

        isActiveAbilityChosen = false;
        ActiveAbilityLineChosen = "";

        isInUtilityRange = false;

        grey = new Color(0.1f, 0.1f, 0.1f, 1f);
        rends = new Renderer[transform.childCount];
        startColors = new Color[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            rends[i] = transform.GetChild(i).GetComponent<Renderer>();
            startColors[i] = rends[i].material.color;
        }

        buildManager = BuildManager.instance;
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if(turret != null && !screenLocker.enabled)
        {
            if(enabled)
                buildManager.SelectNode(this); // CHANGED
            return;
        }

        if (!buildManager.CanBuild)
            return;

        BuildTurret(buildManager.GetTurretToBuild());
        // TODO: Add if statement here to not activate this if ever the tutorial is in process
        buildManager.SetTurretToBuildToNull();
     }

    void BuildTurret(TurretBlueprint blueprint)
    {
        turretBlueprint = blueprint;

        A = DeepCopy(turretBlueprint.A);
        B = DeepCopy(turretBlueprint.B);
        C = DeepCopy(turretBlueprint.C);

        isAMaxed = false;
        isBMaxed = false;
        isCMaxed = false;

        if (PlayerStats.Resources < turretBlueprint.root.cost)
        {
            buildManager.FlashTextDisplay("Not enough money to build that", 1);
            return;
        }

        if (audioSource != null)
        {
            audioSource.clip = buildSound;
            audioSource.Play();
        }

        PlayerStats.Resources -= turretBlueprint.root.cost;

        GameObject _turret = (GameObject)Instantiate(turretBlueprint.root.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        techTree = turretBlueprint.techTree;
        techTreeUI = techTree.ui;
        techTreeUI.SetActive(false);
        currentTS = turretBlueprint.root;
        currentTS.isPurchased = true;
        currentTS.isLocked = false;
        worth += turretBlueprint.root.cost;

        A[0].isLocked = false;
        B[0].isLocked = false;

        if (turretBlueprint.type.Contains("Utility"))
        {
            C[0].isLocked = false;
        }

        if(isInUtilityRange && !turretBlueprint.type.Equals("Utility")) // we don't wanna buff Utility towers
        {
            foreach (UtilityTurret ut in affectingUtilityTurrets)
                ut.AffectNode(this);
        }

        SpecialNode specialNode = gameObject.GetComponent<SpecialNode>();

        if (specialNode != null)
        {
            specialNode.AffectNode(this);
        }

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);
    }

    public void UpgradeTurret(TurretStats turretStats, TurretStats nextTS)
    {
        if (PlayerStats.Resources < turretStats.cost)
        {
            buildManager.FlashTextDisplay("Not enough money to upgrade that!", 1);
            return;
        }

        if (audioSource != null)
        {
            audioSource.clip = upgradeSound;
            audioSource.Play();
        }

        PlayerStats.Resources -= turretStats.cost;
        currentTS = turretStats;
        worth += turretStats.cost;

        // Reverts effect of old turret so buffings don't add up
        if (turretBlueprint.type.Contains("Utility"))
            turret.GetComponent<UtilityTurret>().RevertArea();

        // Get rid of the old turret
        Destroy(turret);

        // Build a new one
        GameObject _turret = (GameObject)Instantiate(turretStats.prefab, GetBuildPosition(), Quaternion.identity);
        turret = _turret;

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        if (isInUtilityRange && !turretBlueprint.type.Equals("Utility")) // we don't wanna buff Utility towers
        {
            foreach (UtilityTurret ut in affectingUtilityTurrets)
                ut.AffectNode(this);
        }

        SpecialNode specialNode = gameObject.GetComponent<SpecialNode>();

        if (specialNode != null)
        {
            specialNode.AffectNode(this);
        }

        turretStats.isPurchased = true;

        if(nextTS != null)
            nextTS.isLocked = false;
    }

    public void SellTurret()
    {
        if (audioSource != null)
        {
            audioSource.clip = sellSound;
            audioSource.Play();
        }

        PlayerStats.Resources += GetSellAmount();

        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 5f);

        if (turretBlueprint.type.Contains("Utility"))
            turret.GetComponent<UtilityTurret>().RevertArea();

        Destroy(turret);
        turretBlueprint = null;

        worth = 0;

        AIndex = 0;
        BIndex = 0;
        CIndex = 0;

        isAMaxed = false;
        isBMaxed = false;
        isCMaxed = false;

        isActiveAbilityChosen = false;
        ActiveAbilityLineChosen = "";
        isUpgraded = false;
        turretLineChosen = "";
    }

    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (!buildManager.CanBuild)
            return;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (buildManager.HasResources)
                rends[i].material.color = rends[i].material.color - grey;
            else
                rends[i].material.color = notEnoughResourcesColor;
        }
    }

    void OnMouseExit()
    {
        if (buildManager.selectedNode == this)
            return;

        for (int i = 0; i < transform.childCount; i++)
        {
            rends[i].material.color = startColors[i];
        }
    }

    public int GetSellAmount()
    {
        return worth / 2;
    }

    public TurretStats[] DeepCopy(TurretStats[] ts)
    {
        TurretStats[] temp = new TurretStats[ts.Length];
        for (int i = 0; i < ts.Length; i++)
        {
            temp[i] = new TurretStats();
            temp[i].name = ts[i].name;
            temp[i].prefab = ts[i].prefab;
            temp[i].cost = ts[i].cost;
            temp[i].isLocked = ts[i].isLocked;
            temp[i].description = ts[i].description;
            temp[i].isPurchased = ts[i].isPurchased;
        }

        return temp;
    }
}
